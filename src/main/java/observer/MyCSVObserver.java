package observer;

import java.lang.reflect.InvocationTargetException;

import environment.Time;
import fr.cemagref.observation.kernel.ObservablesHandler;
import fr.cemagref.observation.observers.ConsoleObserver;
import fr.cemagref.ohoui.annotations.Description;
import fr.cemagref.simaqualife.pilot.Pilot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MyCSVObserver extends ConsoleObserver {

    @Description(name = "Field separator", tooltip = "")
    protected char separator = ';';
    private List<String> observables;
    protected transient List<ObservablesHandler.ObservableFetcher> fetchers;
    /**
     * <code>classObservable</code> is used to display name of attributes as
     * header
     */
    private transient ObservablesHandler classObservable;

    public MyCSVObserver() {
    }

    public MyCSVObserver(boolean sysout, String outputFile) {
        super(sysout, outputFile);
    }

    public MyCSVObserver(boolean sysout, String outputFile, String... obsvervables) {
        super(sysout, outputFile);
        this.observables = Arrays.asList(obsvervables);
    }

    public List<String> getObservables() {
        return observables;
    }

    @Override
    public void addObservable(ObservablesHandler classObservable) {
        ObservablesHandler bak = this.classObservable;
        this.classObservable = classObservable;
        if (bak != null) // The observable has been changed 
        {
            this.init();
        }
    }

    @Override
    public void valueChanged( ObservablesHandler clObservable, Object instance, long t) {
        if (isInDates(t)) {
            StringBuffer buf = new StringBuffer();
            StringBuffer sbSeparator = new StringBuffer(" " + this.separator + " ");
            // print current Time
            //buf.append(t);
            buf.append(Time.getYear(t));
            buf.append(sbSeparator);
            buf.append(Time.getSeason(t));
            // print value of each field
            for (ObservablesHandler.ObservableFetcher fetcher : fetchers) {
                buf.append(sbSeparator);
                Object value = getValue(fetcher, instance);
                buf.append(value == null ? "N/A" : value);
            }
            outputStream.println(buf);
        }
    }

    /**
     *
     * @param fetcher
     * @param instance
     * @return the value returned by the fetcher or null if a problem has
     * occured due to the introspection.
     */
    protected Object getValue(ObservablesHandler.ObservableFetcher fetcher, Object instance) {
        try {
            return fetcher.fetchValue(instance);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(MyCSVObserver.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (IllegalAccessException ex) {
            Logger.getLogger(MyCSVObserver.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (InvocationTargetException ex) {
            Logger.getLogger(MyCSVObserver.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    @Override
    public void init() {
        super.init();
        if (classObservable != null) {
            fetchers = new ArrayList<ObservablesHandler.ObservableFetcher>();
            if (observables != null) {
                for (String name : observables) {
                    fetchers.add(classObservable.getObservableFetcherByName(name));
                }
            } else {
                fetchers.addAll(Arrays.asList(classObservable.getObservableFetchers()));
            }
            // Headers printing
            StringBuffer buf = new StringBuffer();
            StringBuffer sbSeparator = new StringBuffer(" " + this.separator + " ");
            buf.append("Year");
            buf.append(sbSeparator);
            buf.append("Season");
            for (ObservablesHandler.ObservableFetcher fetcher : fetchers) {
                buf.append(sbSeparator);
                buf.append(fetcher.getDescription());
            }
            outputStream.println(buf);
        }
    }

    @Override
    public String toString() {
        if (observables != null) {
        return observables.toString() + " " + super.toString();
        } else {
            return super.toString();
        }
    }
}
