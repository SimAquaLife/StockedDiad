package environment;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import species.DiadromousFish;
import species.DiadromousFish.Gender;
import species.DiadromousFish.Stage;
import fr.cemagref.simaqualife.kernel.spatial.Position;

public class Habitat implements Position {

	public static enum EcologicalFunction {SPAWNING, RIVER_GROWTH, ESTUARINE_GROWTH, OCEAN_GROWTH};

	private final int id;
	private final String name;
	private final double latitude;
	private final double longitude;
	private final EcologicalFunction function;
	private double surface;
	private double habitatEmbryonicSurvival;
	private final double distanceToDownstream;
	private Habitat downstream;
	private Set<Habitat> similars;
	private final String river;

	private transient Set<Habitat> upstreams;
	private transient double distanceToSea;

	private List<DiadromousFish> fishes;

	public Habitat(int id, String name, String river,  double latitude, double longitude,
			double surface, EcologicalFunction function, double distanceToDownstream, double habitatEmbryonicSurvival ) {
		super();
		this.id = id;
		this.name = name;
		this.river = river;
		this.latitude = latitude;
		this.longitude = longitude;
		this.surface = surface;
		this.function = function;
		this.distanceToDownstream = distanceToDownstream;	
		this.habitatEmbryonicSurvival = habitatEmbryonicSurvival;
		upstreams = null;
		similars = null;
	}

	public void initTransientFields() {
		if (upstreams != null) {
			for (Habitat upstream : this.upstreams) {
				upstream.downstream = this;
				upstream.initTransientFields();
			}
		}
	}

	public boolean addUpstream(Habitat habitat) {
		habitat.downstream = this;
		if (upstreams == null) {
			upstreams = new HashSet<Habitat>();
		}
		return upstreams.add(habitat);
	}

	public Set<Habitat> getUpstreams() {
		return upstreams;
	}

	public Habitat getDownstream() {
		return downstream;
	}

	public double getDistanceToSea() {
		return distanceToSea;
	}

	public void setDistanceToSea(double distanceToSea) {
		this.distanceToSea = distanceToSea;
	}

	public double getDistanceToDownstream() {
		return distanceToDownstream;
	}

	public String getName() {
		return name;
	}

	public boolean addSimalar(Habitat habitat) {
		if (similars == null) {
			similars = new HashSet<Habitat>();
		}
		return similars.add(habitat);
	}

	public void setSimilars(Set<Habitat> similars) {
		this.similars = similars;
	}

	public Set<Habitat> getSimilars() {
		return similars;
	}

	public boolean addFish(DiadromousFish fish){
		if ( fishes== null) 		
			fishes = new ArrayList<DiadromousFish>();

		return (fishes.add(fish));
	}

	public boolean removeFish(DiadromousFish fish){
		return (fishes.remove(fish));
	}

	public List<DiadromousFish> getFishes() {
		return fishes;
	}

	public EcologicalFunction getFunction() {
		return function;
	}

	public double getSurface() {
		return surface;
	}

	public void setSurface(double surface) {
		this.surface = surface;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public String getRiver() {
		return river;
	}

	public long getEffective(){
		long eff=0;
		if (fishes != null){
			for(DiadromousFish fish : fishes){
				eff += fish.getAmount();
			}
		}
		return eff;
	}

	public long getFemaleMatureEffective(){
		long eff=0;
		if (fishes != null){
			for(DiadromousFish fish : fishes){
				if (fish.getStage() == Stage.MATURE & fish.getGender() == Gender.FEMALE)
				eff += fish.getAmount();
			}
		}
		return eff;
	}

	
	public long getFemaleEffective(){
		long eff=0;
		if (fishes != null){
			for(DiadromousFish fish : fishes){
				if (fish.getGender() == Gender.FEMALE)
				eff += fish.getAmount();
			}
		}
		return eff;
	}
	

	public double getHabitatEmbryonicSurvival() {
		return habitatEmbryonicSurvival;
	}

	@Override
	public String toString() {
		return "Habitat [id=" + id + ", name=" + name + ", function="
				+ function + "]";
	}


	
	
}
