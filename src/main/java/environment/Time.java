package environment;

import fr.cemagref.simaqualife.pilot.Pilot;

public final class Time {
	
	
	public static enum Season {WINTER, SPRING, SUMMER, AUTOMN};
		
	public static long getCurrentTime(Pilot pilot){
		return pilot.getCurrentTime();
	}
	
	public static Season getSeason(long time){
		return Season.values()[(int)time% Season.values().length];
	}
	
	public static Season getSeason(Pilot pilot){
		return Season.values()[(int) getCurrentTime(pilot)% Season.values().length];
	}
	
	public static long getYear(Pilot pilot){
		return (long) Math.floor(getCurrentTime(pilot)/ Season.values().length);
	}
	
	public static long getYear(long time){
		return (long) Math.floor(time/ Season.values().length);
	}
	public static  double getSeasonDuration(){
		return 1./ Season.values().length;
	}
	

}
