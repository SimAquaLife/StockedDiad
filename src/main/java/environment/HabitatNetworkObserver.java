package environment;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import species.DiadromousFish;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import environment.Habitat.EcologicalFunction;
import fr.cemagref.observation.gui.Configurable;
import fr.cemagref.observation.gui.Drawable;
import fr.cemagref.observation.kernel.ObservablesHandler;
import fr.cemagref.observation.kernel.ObserverListener;
import fr.cemagref.ohoui.filters.NoTransientField;
import fr.cemagref.ohoui.swing.OhOUI;
import fr.cemagref.ohoui.swing.OhOUIDialog;
import fr.cemagref.simaqualife.kernel.util.TransientParameters;
import fr.cemagref.simaqualife.pilot.Pilot;

@SuppressWarnings("serial")
public class HabitatNetworkObserver extends ObserverListener implements Configurable,
Drawable, MouseMotionListener {

	private String title;

	private transient HabitatNetwork bn;
	private transient double minX, minY, rangeX, rangeY;

	// list of reachRect
	private transient Map<Shape, Habitat> mapShapeHabitat;

	// to improve fish point display
	private transient int nbAlea;
	private transient double[] aleaX, aleaY;

	private transient JComponent display;
	private transient JLabel label;
	private transient List<Color> colors;
	// list of color
	//private transient Color[] colorList;

	   protected transient Pilot pilot;

	@Override
	public void addObservable(ObservablesHandler arg0) {
		// nothing to do
	}

	@Override
	public void close() {
		// nothing to do
	}

    @TransientParameters.InitTransientParameters
	public void init(Pilot pilot) {
        this.pilot = pilot;
		// create a uniform generator to  calculate 100 xAlea and yAlea
		// to randomly locate the fish in a habitat
		nbAlea = 1000;
		aleaX=new double[nbAlea];
		aleaY=new double[nbAlea];
		for (int i=0; i < nbAlea; i++){
			aleaX[i]= Math.random();
			aleaY[i]= Math.random();	
		}

		display = new JPanel(new BorderLayout());
		DisplayComponent displayComponent = new DisplayComponent();
		displayComponent.addMouseMotionListener(this);
		displayComponent.setVisible(true);
		displayComponent.setDoubleBuffered(true);
		label = new JLabel("");
		display.add(displayComponent, BorderLayout.CENTER);
		display.add(label, BorderLayout.PAGE_START);


		// Initialize the map linking shape with basin
		mapShapeHabitat = new HashMap<Shape, Habitat>();

		// list of color
		colors= new ArrayList<Color>(25);
		colors.add(new Color(0, 157, 0));
		colors.add(new Color(0, 163, 0));
		colors.add(new Color(0, 168, 0));
		colors.add(new Color(0, 172, 0));
		colors.add(new Color(17, 175, 0));
		colors.add(new Color(57, 178, 0));
		colors.add(new Color(82, 179, 0));
		colors.add(new Color(103, 180, 0));
		colors.add(new Color(122, 179, 0));
		colors.add(new Color(138, 178, 39));
		colors.add(new Color(153, 175, 117));
		colors.add(new Color(165, 173, 153));
		colors.add(new Color(171, 171, 171));
		colors.add(new Color(183, 167, 157));
		colors.add(new Color(201, 159, 127));
		colors.add(new Color(219, 149, 78));
		colors.add(new Color(235, 139, 0));
		colors.add(new Color(248, 127, 0));
		colors.add(new Color(255, 114, 0));
		colors.add(new Color(255, 101, 0));
		colors.add(new Color(255, 87, 0));
		colors.add(new Color(255, 72, 0));		
		colors.add(new Color(255, 57, 0));
		colors.add(new Color(255, 39, 0));
		colors.add(new Color(255, 14, 0));

	}

	@Override
	public void valueChanged(ObservablesHandler arg0, Object arg1, long arg2) {
		display.repaint();
		String txt= ("start of " + new Long(Time.getYear(pilot))).toString() + (" ") +
				Time.getSeason(pilot).toString();
		label.setText(txt);
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		// nothing to do
	}

	/* (non-Javadoc)
	 * @see java.awt.event.MouseMotionListener#mouseMoved(java.awt.event.MouseEvent)
	 */
	/* (non-Javadoc)
	 * @see java.awt.event.MouseMotionListener#mouseMoved(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseMoved(MouseEvent e) {
		int y = (e.getY());
		int x = (e.getX());
		Habitat habitat;

		String txt= ("start of " + new Long(Time.getYear(pilot))).toString() + (" ") +
				Time.getSeason(pilot).toString()+ " - ";

		for (Shape shape : mapShapeHabitat.keySet()) {
			if (shape.contains(x, y)) {
				habitat = mapShapeHabitat.get(shape);
				txt +=habitat.getRiver()+"-"+habitat.getName()+ " [" + habitat.getFunction() + "] "+
				habitat.getEffective() + " including " + habitat.getFemaleEffective() + "  females including " +
				habitat.getFemaleMatureEffective() + " mature fish";
						
				/*				for (DiadromousFishGroup group : habitat.getGroups()){
					txt += group.getName() +"=" + habitat.getEffective(group)+" in " +
							habitat.getSuperFishNumber(group) + "  SI ";
				}*/
				break;
			}
		}
		label.setText(txt);
	}

	@Override
	public JComponent getDisplay() {
		bn = (HabitatNetwork) pilot.getAquaticWorld().getEnvironment();

		// compute min and max of x and y 
		double maxX, maxY;
		maxX = maxY = Double.NEGATIVE_INFINITY;
		minX = minY = Double.POSITIVE_INFINITY;

		rangeX = rangeY = 0.;
		// System.out.println(esu.toString());
		for (Habitat habitat : bn.getHabitats()) {
			// take the opposite of y coordinates because positive values in
			// a panel are to the south
			minX = Math.min(minX, getHabitatShape(habitat).getBounds2D().getMinX());
			maxX = Math.max(maxX, getHabitatShape(habitat).getBounds2D().getMaxX());
			minY = Math.min(minY, getHabitatShape(habitat).getBounds2D().getMinY());
			maxY = Math.max(maxY, getHabitatShape(habitat).getBounds2D().getMaxY());
		}

		// compute the range with a small margin
		rangeX = maxX - minX;
		minX -= 0.02 * rangeX;
		maxX += 0.02 * rangeX;
		rangeX = maxX - minX;

		rangeY = maxY - minY;
		minY -= 0.02 * rangeY;
		maxY += 0.02 * rangeY;
		rangeY = maxY - minY;

		//System.out.println("ranges of network for display: "+rangeX+" "+rangeY);

		return display;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public void configure() {
		OhOUIDialog dialog = OhOUI
				.getDialog(null, this, new NoTransientField());
		dialog.setSize(new Dimension(500, 500));
		dialog.setVisible(true);
		//display.repaint();
	}

	public void disable() {
		display.setVisible(false);
	}

	public static void main(String[] args) { System.out.println((new
			XStream(new DomDriver())) .toXML(new HabitatNetworkObserver())); }

	private class DisplayComponent extends JComponent {

		@Override
		protected synchronized void paintComponent(Graphics g) {

			double W =this.getWidth();
			double H =this.getHeight();
			AffineTransform af = new AffineTransform(W/rangeX, 0., 0., 
					-H/rangeY, -W*minX/rangeX, H*(1. + minY/rangeY));
			//System.out.println(af.toString());

			// prepare the graphics
			this.paintComponents(g);
			Graphics2D g2d = (Graphics2D) g;
			g2d.setStroke(new BasicStroke(3)); // define the line

			// Draw Background
			g.setColor(Color.WHITE);
			g.fillRect(0, 0, (int) W, (int) H);

			// prepare the diameter for the fish circle
			double fishX, fishY;
			int fishDiameter = (int) Math.round(Math.min(W,H) / 100.);

			mapShapeHabitat.clear();

			//draw the network as line
			g.setColor(Color.BLUE);
			Path2D.Double displayHabitatNetworkShape = getHabitatNetworkShape(bn.getRoot()) ;
			
			g.setColor(colors.get(0));		
			g2d.draw(displayHabitatNetworkShape.createTransformedShape(af));
		
			for (Habitat habitat : bn.getHabitats()) {
				// draw each basin
				Shape displayShape = 
						new Path2D.Double(getHabitatShape(habitat)).createTransformedShape(af);
				mapShapeHabitat.put(displayShape, habitat); // to be used by the mouseMoved()

				// fill the shape of the habitat
				if (habitat.getFunction() == EcologicalFunction.SPAWNING)
					g.setColor(new Color(0, 204, 255));
				else
					g.setColor(new Color(0, 102, 255));
				g2d.fill(displayShape);

				// draw the contour of the habitat
				g.setColor(colors.get(0));
				g2d.draw(displayShape);

				// draw fish
				int cpt=0;
				if( habitat.getFishes() != null) {
					for (DiadromousFish fish : habitat.getFishes()){
						do {
							fishX = displayShape.getBounds2D().getMinX() + displayShape.getBounds2D().getWidth()
									* (0.2 + .6 * aleaX[cpt % nbAlea]); //- fishDiameter/2;
							fishY =  displayShape.getBounds2D().getMinY()+ displayShape.getBounds2D().getHeight()
									* (0.2 + .6 * aleaY[cpt % nbAlea]);// - fishDiameter/2;
							//System.out.println(! displayShape.contains(fishX, fishY));
							cpt++;
						} while (! displayShape.contains(fishX, fishY));
						//idxColor = bn.getRow(fish.getBirthBasin().getId()) % colors.size();
						g.setColor(Color.RED);
						g2d.fillOval((int) fishX, (int) fishY, fishDiameter,
								fishDiameter);
					}
				}
			}
		}
	}

	private Shape getHabitatShape (Habitat habitat) {
		double surfaceScale=500.;
		double ratioWidthHeight = 6.;
		double width = Math.sqrt(habitat.getSurface()*surfaceScale /ratioWidthHeight);
		double height = ratioWidthHeight * width;
/*		switch(habitat.getFunction()){
		case SPAWNING :
			width = Math.sqrt(habitat.getSurface()*surfaceScale);
			height = width;
			break;
		case NURSERY :
			height = Math.pow(habitat.getSurface()*surfaceScale,1./3.);
			width = 3. * height;
			break;
		case JUVENILE_GROWTH :
			width = Math.pow(habitat.getSurface()*surfaceScale,1./4.);
			height = 3. * width;
			break;
		case ADULT_GROWTH :
			width = Math.pow(habitat.getSurface()*surfaceScale,1./4.);
			height = 3. * width;
			break;
		}*/

		double x= habitat.getLongitude() - width/2.;
		double y = habitat.getLatitude() - height/2.;

		Rectangle2D.Double  rect= new Rectangle2D.Double(x, y, width, height);
		return rect;
	}
	
	private void getHNShape(Habitat habitat, Path2D.Double HNshape){

		if (habitat.getUpstreams() != null) {
			for(Habitat hab: habitat.getUpstreams()){
				HNshape.moveTo(habitat.getLongitude(), habitat.getLatitude());
				HNshape.lineTo(hab.getLongitude(), hab.getLatitude());
				
				this.getHNShape(hab, HNshape);
			}
		}
	}
	
	private Path2D.Double getHabitatNetworkShape(Habitat habitat){
		 Path2D.Double HNshape = new  Path2D.Double();
		 getHNShape(habitat, HNshape);
		 return HNshape;
	}

	/* (non-Javadoc)
	 * @see fr.cemagref.observation.kernel.ObserverListener#init()
	 */
	@Override
	public void init() {
		// nothing to do
		
	}

}






