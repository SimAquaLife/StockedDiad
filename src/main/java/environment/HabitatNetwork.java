package environment;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Pattern;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import environment.Habitat.EcologicalFunction;
import fr.cemagref.simaqualife.kernel.AquaNismsGroup;
import fr.cemagref.simaqualife.kernel.spatial.Environment;
import fr.cemagref.simaqualife.kernel.util.TransientParameters;
import fr.cemagref.simaqualife.kernel.util.TransientParameters.InitTransientParameters;
import fr.cemagref.simaqualife.pilot.Pilot;
import species.DiadromousFish;

public class HabitatNetwork extends Environment<Habitat, DiadromousFish> {

	private String habitatNetworkFileName ="data/input/GirondeSturgeon.csv";
	
	protected transient Habitat root;
	protected transient Map<String, Habitat> mapNameHabitat;

	public static void main(String[] args) { 
	System.out.println((new
			XStream(new DomDriver())) .toXML(new HabitatNetwork())); 
	}

	@InitTransientParameters
	public void initTransientParameters(Pilot pilot) {

		mapNameHabitat = new HashMap<String, Habitat>();

		String name, river;
		EcologicalFunction function;
		double longitude, latitude, habitatQuality;
		double surface =0.,distanceToDownstream=0.;
		int id, idDownstream;
		FileReader reader;
		Scanner scanner;

		Map<Integer, Habitat> mapIdHabitat = new HashMap<Integer, Habitat>();
		Map<Integer, Integer> mapIdIdDownstream = new HashMap<Integer, Integer>();
		Map<String, Set<Integer>> mapConfusionIds = new HashMap<String, Set<Integer>>();
		Map<Integer, String> mapIdConfusion = new HashMap<Integer, String>();
		try {
			// open the file
			reader = new FileReader(habitatNetworkFileName);

			// Parsing the file
			scanner = new Scanner(reader);
			scanner.useLocale(Locale.ENGLISH); // to have a point as decimal separator !!!
			scanner.useDelimiter(Pattern.compile("[,;\n]"));

			// skip the first line
			scanner.nextLine();
			while (scanner.hasNext()) {
				//System.out.println(scanner.next());
				id = scanner.nextInt();
				name = scanner.next();
				longitude= scanner.nextDouble(); // x axis
				latitude = scanner.nextDouble(); // y axis
				scanner.next();
				scanner.next();
				surface = scanner.nextDouble();
				function = EcologicalFunction.valueOf(scanner.next());
				idDownstream = scanner.nextInt();
				distanceToDownstream =  scanner.nextDouble();
				river = scanner.next();
				habitatQuality = scanner.nextDouble();
				// skip unused data
				for (int i=0; i<5; i++){
					scanner.next();
				}

				// create the habitat
				Habitat habitat = new Habitat(id, name, river, latitude, longitude, surface, 
						function, distanceToDownstream, habitatQuality);

				mapNameHabitat.put(name, habitat);

				// connect habitat id
				mapIdHabitat.put(id, habitat);
				if (idDownstream != -1)
					mapIdIdDownstream.put(id, idDownstream);

				// list similar habitat id
				mapIdConfusion.put(id, river);
				Set<Integer> similarIds = mapConfusionIds.get(river);
				if (similarIds==null){
					similarIds = new HashSet<Integer>();
					mapConfusionIds.put(river, similarIds);
				}

				similarIds.add(id);
			}

			reader.close();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}

		// create the habitat network
		for (Integer ind : mapIdHabitat.keySet()) {
			Habitat habitat = mapIdHabitat.get(ind);
			Integer indDownstream = mapIdIdDownstream.get(ind);
			Habitat downstreamHabitat = mapIdHabitat.get(indDownstream);

			// connect downstream habitat
			if (downstreamHabitat != null)
				downstreamHabitat.addUpstream(habitat);
			else 
				this.root = habitat;

			// connect similar habitats
			String confusion = mapIdConfusion.get(ind);
			for (Integer index : mapConfusionIds.get(confusion)){
				habitat.addSimalar(mapIdHabitat.get(index));
			}
		}

		// compute habitats characteristics
		computeDistanceToSea(root);
	}

	private void computeDistanceToSea(Habitat habitat) {
		// the most downstream habitat corresponds to the sea
		double dist = (habitat.getDownstream() == null ? 0.
				: habitat.getDownstream().getDistanceToSea() + habitat.getDistanceToDownstream());
		habitat.setDistanceToSea(dist);

		if (habitat.getUpstreams() != null) {
			for (Habitat upstream : habitat.getUpstreams()) {
				computeDistanceToSea(upstream);
			}
		}
	}

	private void getAllUpstreams(Habitat habitat, List<Habitat> allUpstreams) {
		allUpstreams.add(habitat);
		if (habitat.getUpstreams() != null) {
			for (Habitat upstream : habitat.getUpstreams()) {
				getAllUpstreams(upstream, allUpstreams);
			}
		}
	}

	public List<Habitat> getHabitats() {
		List<Habitat> allUpstreams = new ArrayList<Habitat>();
		getAllUpstreams(this.root, allUpstreams);
		return allUpstreams;
	}


	private void getAllSpwaningGrounds(Habitat habitat, List<Habitat> allSpawnings) {
		if (habitat.getFunction() == EcologicalFunction.SPAWNING)
			allSpawnings.add(habitat);
		if (habitat.getUpstreams() != null) {
			for (Habitat upstream : habitat.getUpstreams()) {
				getAllUpstreams(upstream, allSpawnings);
			}
		}
	}

	public List<Habitat> getSpwaningGrounds() {
		List<Habitat> allSpawnings = new ArrayList<Habitat>();
		getAllSpwaningGrounds(this.root, allSpawnings);
		return allSpawnings;
	}
	@Override
	public void addAquaNism(DiadromousFish arg0, AquaNismsGroup arg1) {
		// TODO Auto-generated method stub

	}


	@Override
	public List<Habitat> getNeighbours(Habitat habitat) {
		List<Habitat> neighbours = new ArrayList<Habitat>();
		if (habitat.getDownstream() != null)
			neighbours.add(habitat.getDownstream());
		if (!habitat.getUpstreams().isEmpty()){
			for (Habitat hab : habitat.getUpstreams())
				neighbours.add(hab);
		}
		return neighbours;
	}

	@Override
	public void moveAquaNism(DiadromousFish arg0, AquaNismsGroup arg1,
			Habitat arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeAquaNism(DiadromousFish arg0, AquaNismsGroup arg1) {
		// TODO Auto-generated method stub

	}

	public Habitat getRoot() {
		return root;
	}

	public Habitat getHabitat(String habitatName){
		return mapNameHabitat.get(habitatName);
	}


	public static Habitat getNextDownstreamFunctionalHabitat(Habitat habitat, EcologicalFunction function){
		Habitat downstream = habitat.getDownstream();
		if(downstream != null){
			if (downstream.getFunction() == function)
				return downstream;
			else 
				return getNextDownstreamFunctionalHabitat (downstream, function);
		}
		else
			return null;
	}

	// example of recursive function by Thibaud LAMBERT
	public static Habitat getHabitatByName(Habitat habitat, String name){
		if(habitat.getName() == name)
			return habitat;
		else {
			Set<Habitat> upstreams= habitat.getUpstreams();
			if (upstreams == null)
				return null;
			else {
				for (Habitat upstream : upstreams){
					Habitat hab = getHabitatByName(upstream, name);
					if(hab != null)
						return habitat;
				}
				return null;
			}
		}
	}
}


