/**
 * 
 */
package species;

import org.openide.util.lookup.ServiceProvider;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import environment.Time;
import environment.Habitat.EcologicalFunction;
import environment.Time.Season;
import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;

/**
 * @author Patrick.Lambert
 * 
 */

@ServiceProvider(service = AquaNismsGroupProcess.class)
public class MigrateToBirthHabitat extends
AquaNismsGroupProcess<DiadromousFish, DiadromousFishGroup> {

	private Season reproductionMigrationSeason = Season.SPRING;

	@Override
	public void doProcess(DiadromousFishGroup group) {
		if (Time.getSeason(group.getPilot()) == reproductionMigrationSeason){
			for (DiadromousFish fish : group.getAquaNismsList()){
				if (fish.isReadyToSpawn() 
						& fish.getPosition().getFunction()== EcologicalFunction.OCEAN_GROWTH)
					fish.moveTo(pilot, fish.getBirthHabitat(), group);
			}
		}
	}

	public static void main(String[] args) { System.out.println((new
			XStream(new DomDriver())) .toXML(new MigrateToBirthHabitat())); }
}
