/**
 * 
 */
package species;

import java.util.ArrayList;
import java.util.List;

import miscellaneous.Miscellaneous;

import org.openide.util.lookup.ServiceProvider;

import species.DiadromousFish.Gender;
import species.DiadromousFish.Stage;
import umontreal.iro.lecuyer.probdist.GammaDist;
import umontreal.iro.lecuyer.probdist.PoissonDist;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import environment.Time;
import environment.Time.Season;
import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;
import fr.cemagref.simaqualife.kernel.util.TransientParameters.InitTransientParameters;
import fr.cemagref.simaqualife.pilot.Pilot;

/**
 * @author Patrick.Lambert
 * 
 */

@ServiceProvider(service = AquaNismsGroupProcess.class)
public class Mature extends
AquaNismsGroupProcess<DiadromousFish, DiadromousFishGroup> {

	private Season maturationSeason = Season.SPRING;

	private double femaleMinimumAgeAtMaturation = 8.; // in year
	private double femaleMeanAgeAtMaturation = 14.; // in year
	private double femaleSdAgeAtMaturation = 1.;
	private double femaleMeanInterSpawningInterval = 4.;

	private double maleMinimumAgeAtMaturation = 6.; // in year
	private double maleMeanAgeAtMaturation = 8; // in year
	private double maleSdAgeAtMaturation = .5;
	private double maleMeanInterSpawningInterval = 1.2;


	private transient GammaDist femaleMaturationDist;
	private transient GammaDist maleMaturationDist;
	private transient PoissonDist femaleInterSpawningIntervalDist;
	private transient PoissonDist maleInterSpawningIntervalDist;

	public static void main(String[] args) { System.out.println((new
			XStream(new DomDriver())) .toXML(new Mature())); }

	@Override
	@InitTransientParameters
	public void initTransientParameters(Pilot pilot) {

		// for female maturation
		double alpha, lambda;
		alpha = Math.pow( (femaleMeanAgeAtMaturation - femaleMinimumAgeAtMaturation)
				/ femaleSdAgeAtMaturation, 2);
		lambda = (femaleMeanAgeAtMaturation - femaleMinimumAgeAtMaturation)
				/ Math.pow(femaleSdAgeAtMaturation, 2);
		femaleMaturationDist = new GammaDist(alpha, lambda);

		// for male maturation
		alpha = Math.pow( (maleMeanAgeAtMaturation - maleMinimumAgeAtMaturation)
				/ maleSdAgeAtMaturation, 2);
		lambda = (maleMeanAgeAtMaturation - maleMinimumAgeAtMaturation)
				/ Math.pow(maleSdAgeAtMaturation, 2);
		maleMaturationDist = new GammaDist(alpha, lambda);
		
		femaleInterSpawningIntervalDist = new PoissonDist(femaleMeanInterSpawningInterval);
		maleInterSpawningIntervalDist = new PoissonDist(maleMeanInterSpawningInterval);
	}

	@Override
	public void doProcess(DiadromousFishGroup group) {

		if (Time.getSeason(group.getPilot()) == maturationSeason){
			double age, timeFromLastReproduction, probaToMature;
			long amountToMature, amountToStay;
			List<DiadromousFish> fishToBeAdded = new ArrayList<DiadromousFish>();
			
			for (DiadromousFish fish : group.getAquaNismsList()) {
				age = fish.getAge();
				
				// new maturation
				if (fish.getStage() == Stage.MATURE){
					timeFromLastReproduction = age - fish.getAgeAtLastReproduction();
					if (fish.getGender() == Gender.FEMALE){
						probaToMature = (femaleInterSpawningIntervalDist.cdf(timeFromLastReproduction) - 
								femaleInterSpawningIntervalDist.cdf(timeFromLastReproduction -1.))
								/ (1 - femaleInterSpawningIntervalDist.cdf(timeFromLastReproduction - 1.));
					}
					else {
						probaToMature = (maleInterSpawningIntervalDist.cdf(timeFromLastReproduction) - 
								maleInterSpawningIntervalDist.cdf(timeFromLastReproduction -1.))
								/ (1 - maleInterSpawningIntervalDist.cdf(timeFromLastReproduction - 1.));
					}
					
					amountToMature = Miscellaneous.binomialForSuperIndividual(group.getPilot(), fish.getAmount(), probaToMature);
					amountToStay= fish.getAmount() - amountToMature;

					if (amountToMature != 0){
						if (amountToStay == 0){
							fish.setReadyToSpawn(true);
						}
						else {
							// create a new SI for the proportion of fish that mature
							DiadromousFish newFish= fish.cloneWithNewPositionAndAmount(fish.getPosition(), amountToMature);
							newFish.setReadyToSpawn(true);
							fishToBeAdded.add(newFish);

							// update the fish that stay immature
							fish.setAmount(amountToStay);	
						}
					}					
				}
				
				// first maturation
				else if (fish.getStage() == Stage.SUBADULT) {
					if (fish.getGender() == Gender.FEMALE){
						// compute the prob to mature between age and age-1 
						// knowing that it was not mature before
						probaToMature = (femaleMaturationDist.cdf(age - femaleMinimumAgeAtMaturation) - 
								femaleMaturationDist.cdf(age - femaleMinimumAgeAtMaturation -1.))
								/ (1 - femaleMaturationDist.cdf(age - femaleMinimumAgeAtMaturation - 1.));
						
						//System.out.println(fish.getGender() +": "+fish.getAge() + "-->"+ probaToMature);
					}
					else {
						probaToMature = (maleMaturationDist.cdf(age - maleMinimumAgeAtMaturation) - 
								maleMaturationDist.cdf(age - maleMinimumAgeAtMaturation -1.))
								/ (1 - maleMaturationDist.cdf(age - maleMinimumAgeAtMaturation - 1.));
					}
					
					//System.out.println(fish.getGender() +": "+fish.getAge() + "-->"+ probaToMature);

					amountToMature = Miscellaneous.binomialForSuperIndividual(group.getPilot(), fish.getAmount(), probaToMature);
					amountToStay= fish.getAmount() - amountToMature;

					if (amountToMature != 0){
						if (amountToStay == 0){
							fish.setStage(Stage.MATURE);
							fish.setReadyToSpawn(true);
						}
						else {
							// create a new SI for the proportion of fist that mature
							DiadromousFish newFish= fish.cloneWithNewPositionAndAmount(fish.getPosition(), amountToMature);
							newFish.setStage(Stage.MATURE);
							newFish.setReadyToSpawn(true);
							fishToBeAdded.add(newFish);

							// update the fist that stay immature
							fish.setAmount(amountToStay);	
						}
					}
				}

			}
			
			if (! fishToBeAdded.isEmpty()){
				for (DiadromousFish fish : fishToBeAdded){
					group.addAquaNism(fish);
				}
			}
		}
		
		//System.out.println(this.getClass()+ " is finished");
	}
}
