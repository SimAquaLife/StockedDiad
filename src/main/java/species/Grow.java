package species;


import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import environment.Time;
import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;
import fr.cemagref.simaqualife.kernel.util.TransientParameters.InitTransientParameters;
import fr.cemagref.simaqualife.pilot.Pilot;


import org.openide.util.lookup.ServiceProvider;

import umontreal.iro.lecuyer.probdist.NormalDist;
import umontreal.iro.lecuyer.randvar.NormalGen;


@ServiceProvider(service = AquaNismsGroupProcess.class)
public class Grow extends AquaNismsGroupProcess<DiadromousFish, DiadromousFishGroup> {

	public double K = 0.3; // en year-1
	public double sigmaDeltaL = 0.2; // random value... has to be fixed with literature 
	public double Linf =150; // en cm

	private transient NormalGen genNormal;

	public static void main(String[] args) { System.out.println((new
			XStream(new DomDriver())) .toXML(new Grow())); }

	@Override
	@InitTransientParameters
	public void initTransientParameters(Pilot pilot) {
		genNormal = new NormalGen( pilot.getRandomStream(),
				new NormalDist(0., 1.));
	}

	@Override
	public void doProcess(DiadromousFishGroup group) {
	
			double muDeltaL = 0.;
			double growthIncrement = 0.;

			for (DiadromousFish fish : group.getAquaNismsList()){
			if (fish.getLength() < Linf){
				muDeltaL = Math.log((Linf - fish.getLength()) * (1 - Math.exp(-K * Time.getSeasonDuration())))
						- (Math.pow(sigmaDeltaL,2))/2;
				growthIncrement = Math.exp(genNormal.nextDouble()*sigmaDeltaL + muDeltaL);
				fish.setLength(Math.min(Linf, fish.getLength() + growthIncrement));											
			}else{
				fish.setLength(Linf);
			}
		}
	}
}
