package species;


import java.util.List;

import species.DiadromousFish.Gender;
import species.DiadromousFish.Origin;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import environment.Habitat;
import environment.Time;

import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;
import fr.cemagref.simaqualife.kernel.util.TransientParameters.InitTransientParameters;
import fr.cemagref.simaqualife.pilot.Pilot;

public class MultipleReleaseFish extends AquaNismsGroupProcess<DiadromousFish, DiadromousFishGroup> {

	private List<Integer> releaseYears;
	private List<Release> releases;
	private long releaseNumber = 50000;

	private double proportionOfFemale =.5;
	private boolean addDifferent = true;

	private transient long amountInSuperIndividual;

	@Override
	@InitTransientParameters
	public void initTransientParameters(Pilot pilot) {
		super.initTransientParameters(pilot);
		amountInSuperIndividual = -1;
		//releaseYears = new ArrayList<Integer>();
		//releases = new ArrayList<Release>();
		if (releaseNumber > 0){
			for (Release release : releases){
				release.setNumber(releaseNumber);
			}
		}
	}

	@Override
	public void doProcess(DiadromousFishGroup group) {

		if (amountInSuperIndividual == -1){
			if (group.getMeanSurvival() == 0.)
				amountInSuperIndividual= group.getDefaultAmountInSuperIndividual();
			else
				amountInSuperIndividual =  (long) (1./(group.getMeanLifespan() * group.getMeanSurvival()));
		}

		if (releaseYears == null | releaseYears.contains(Time.getYear(group.getPilot()))) {
			for (Release action : releases) {
				if (Time.getSeason(group.getPilot()) == action.getSeason()){
					Habitat releaseHabitat = group.getEnvironment().getHabitat(action.getLocation());

					long numberOfFemale = Math.round((action.getNumber()) * proportionOfFemale) ;
					long numberOfMale = action.getNumber() - numberOfFemale;	

					// female
					long numberOfsuperIndividual = (long) Math.floor((double) numberOfFemale / (double) amountInSuperIndividual);
					for (long i=0; i<numberOfsuperIndividual; i++){
						if(addDifferent)
							group.addDifferentAquaNism(new DiadromousFish(group.getPilot(), releaseHabitat, action.getStage(), Gender.FEMALE, amountInSuperIndividual,
									action.getAge(), action.getLength(), Origin.STOCKED));
						else
							group.addAquaNism(new DiadromousFish(group.getPilot(), releaseHabitat, action.getStage(), Gender.FEMALE, amountInSuperIndividual,
									action.getAge(), action.getLength(), Origin.STOCKED));
					}
					if (amountInSuperIndividual * numberOfsuperIndividual < numberOfFemale  ) {
						if(addDifferent)
							group.addDifferentAquaNism(new DiadromousFish(group.getPilot(), releaseHabitat, action.getStage(), Gender.FEMALE,
									numberOfFemale - amountInSuperIndividual * numberOfsuperIndividual ,
									action.getAge(), action.getLength(), Origin.STOCKED));
						else
							group.addAquaNism(new DiadromousFish(group.getPilot(), releaseHabitat, action.getStage(), Gender.FEMALE,
									numberOfFemale - amountInSuperIndividual * numberOfsuperIndividual ,
									action.getAge(), action.getLength(), Origin.STOCKED));
					}

					// male
					numberOfsuperIndividual = (long) Math.floor((double) numberOfMale / (double) amountInSuperIndividual);
					for (long i=0; i<numberOfsuperIndividual; i++){
						if(addDifferent)
							group.addDifferentAquaNism(new DiadromousFish(group.getPilot(), releaseHabitat, action.getStage(), Gender.MALE, amountInSuperIndividual,
									action.getAge(), action.getLength(), Origin.STOCKED));
						else
							group.addAquaNism(new DiadromousFish(group.getPilot(), releaseHabitat, action.getStage(), Gender.MALE, amountInSuperIndividual,
									action.getAge(), action.getLength(), Origin.STOCKED));
					}
					if (amountInSuperIndividual * numberOfsuperIndividual < numberOfMale  ) {
						if(addDifferent)
							group.addDifferentAquaNism(new DiadromousFish(group.getPilot(), releaseHabitat, action.getStage(), Gender.MALE,
									numberOfMale - amountInSuperIndividual * numberOfsuperIndividual , 
									action.getAge(), action.getLength(), Origin.STOCKED));
						else
							group.addAquaNism(new DiadromousFish(group.getPilot(), releaseHabitat, action.getStage(), Gender.MALE,
									numberOfMale - amountInSuperIndividual * numberOfsuperIndividual , 
									action.getAge(), action.getLength(), Origin.STOCKED));
					}
				}	
			}
		}
	}

	public static void main(String[] args) { System.out.println((new
			XStream(new DomDriver())) .toXML(new MultipleReleaseFish())); }
}
