/**
 * 
 */
package species;

import java.util.ArrayList;
import java.util.List;

import miscellaneous.Miscellaneous;
import miscellaneous.TruncatedGammaDist;

import org.openide.util.lookup.ServiceProvider;

import species.DiadromousFish.Gender;
import species.DiadromousFish.Stage;
import umontreal.iro.lecuyer.probdist.PoissonDist;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import environment.Time;
import environment.Time.Season;
import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;
import fr.cemagref.simaqualife.kernel.util.TransientParameters.InitTransientParameters;
import fr.cemagref.simaqualife.pilot.Pilot;

/**
 * @author Patrick.Lambert
 * 
 */

@ServiceProvider(service = AquaNismsGroupProcess.class)
public class MatureWithTruncatedDistribution extends
AquaNismsGroupProcess<DiadromousFish, DiadromousFishGroup> {

	private Season maturationSeason = Season.SPRING;

	private double probTruncature = 0.01;
	
	private double femaleMeanAgeAtMaturation = 14.; // in year
	private double femaleSdAgeAtMaturation = 1.;
	private double femaleMeanInterSpawningInterval = 4.;

	private double maleMeanAgeAtMaturation = 8; // in year
	private double maleSdAgeAtMaturation = .5;
	private double maleMeanInterSpawningInterval = 1.2;

	private transient double lag;
	private transient TruncatedGammaDist femaleMaturationDist;
	private transient TruncatedGammaDist maleMaturationDist;
	private transient PoissonDist femaleInterSpawningIntervalDist;
	private transient PoissonDist maleInterSpawningIntervalDist;

	public static void main(String[] args) { System.out.println((new
			XStream(new DomDriver())) .toXML(new MatureWithTruncatedDistribution())); }

	@Override
	@InitTransientParameters
	public void initTransientParameters(Pilot pilot) {

		// for female maturation
		double alpha, lambda;
		alpha = Math.pow( femaleMeanAgeAtMaturation	/ femaleSdAgeAtMaturation, 2);
		lambda = femaleMeanAgeAtMaturation /Math.pow(femaleSdAgeAtMaturation, 2);
		femaleMaturationDist = new TruncatedGammaDist(alpha, lambda, probTruncature);

		// for male maturation
		alpha = Math.pow(maleMeanAgeAtMaturation / maleSdAgeAtMaturation, 2);
		lambda = maleMeanAgeAtMaturation / Math.pow(maleSdAgeAtMaturation, 2);
		maleMaturationDist = new TruncatedGammaDist(alpha, lambda, probTruncature);
		
		lag =1.; // to avoid forgetting the proba for interval =0
		femaleInterSpawningIntervalDist = new PoissonDist(femaleMeanInterSpawningInterval-lag);
		maleInterSpawningIntervalDist = new PoissonDist(maleMeanInterSpawningInterval-lag);
	}

	@Override
	public void doProcess(DiadromousFishGroup group) {

		if (Time.getSeason(group.getPilot()) == maturationSeason){
			double age, laggedTimeFromLastReproduction, probaToMature;
			long amountToMature, amountToStay;
			List<DiadromousFish> fishToBeAdded = new ArrayList<DiadromousFish>();
			
			for (DiadromousFish fish : group.getAquaNismsList()) {
				age = fish.getAge();
				
				// new maturation
				if (fish.getStage() == Stage.MATURE){
					laggedTimeFromLastReproduction = age - fish.getAgeAtLastReproduction() - lag;
					if (fish.getGender() == Gender.FEMALE){
						probaToMature = (femaleInterSpawningIntervalDist.cdf(laggedTimeFromLastReproduction) - 
								femaleInterSpawningIntervalDist.cdf(laggedTimeFromLastReproduction -1.))
								/ (1 - femaleInterSpawningIntervalDist.cdf(laggedTimeFromLastReproduction - 1.));
					}
					else {
						probaToMature = (maleInterSpawningIntervalDist.cdf(laggedTimeFromLastReproduction) - 
								maleInterSpawningIntervalDist.cdf(laggedTimeFromLastReproduction -1.))
								/ (1 - maleInterSpawningIntervalDist.cdf(laggedTimeFromLastReproduction - 1.));
					}
					
					amountToMature = Miscellaneous.binomialForSuperIndividual(group.getPilot(), fish.getAmount(), probaToMature);
					amountToStay= fish.getAmount() - amountToMature;

					if (amountToMature != 0){
						if (amountToStay == 0){
							fish.setReadyToSpawn(true);
						}
						else {
							// create a new SI for the proportion of fish that mature
							DiadromousFish newFish= fish.cloneWithNewPositionAndAmount(fish.getPosition(), amountToMature);
							newFish.setReadyToSpawn(true);
							fishToBeAdded.add(newFish);

							// update the fish that stay immature
							fish.setAmount(amountToStay);	
						}
					}					
				}
				
				// first maturation
				else if (fish.getStage() == Stage.SUBADULT) {
					if (fish.getGender() == Gender.FEMALE){
						// compute the prob to mature between age and age-1 
						// knowing that it was not mature before
						probaToMature = (femaleMaturationDist.cdf(age) - femaleMaturationDist.cdf(age -1.))
								/ (1 - femaleMaturationDist.cdf(age - 1.));
						
						//System.out.println(fish.getGender() +": "+fish.getAge() + "-->"+ probaToMature);
					}
					else {
						probaToMature = (maleMaturationDist.cdf(age) - maleMaturationDist.cdf(age -1.))
								/ (1 - maleMaturationDist.cdf(age - 1.));
					}
					
					//System.out.println(fish.getGender() +": "+fish.getAge() + "-->"+ probaToMature);

					amountToMature = Miscellaneous.binomialForSuperIndividual(group.getPilot(), fish.getAmount(), probaToMature);
					amountToStay= fish.getAmount() - amountToMature;

					if (amountToMature != 0){
						if (amountToStay == 0){
							fish.setStage(Stage.MATURE);
							fish.setReadyToSpawn(true);
						}
						else {
							// create a new SI for the proportion of fist that mature
							DiadromousFish newFish= fish.cloneWithNewPositionAndAmount(fish.getPosition(), amountToMature);
							newFish.setStage(Stage.MATURE);
							newFish.setReadyToSpawn(true);
							fishToBeAdded.add(newFish);

							// update the fist that stay immature
							fish.setAmount(amountToStay);	
						}
					}
				}
			}
			
			if (! fishToBeAdded.isEmpty()){
				for (DiadromousFish fish : fishToBeAdded){
					group.addAquaNism(fish);
				}
			}
		}
		
		//System.out.println(this.getClass()+ " is finished");
	}
}
