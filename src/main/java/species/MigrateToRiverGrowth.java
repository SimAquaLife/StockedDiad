/**
 * 
 */
package species;

import org.openide.util.lookup.ServiceProvider;

import species.DiadromousFish.Stage;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import environment.Habitat;
import environment.Habitat.EcologicalFunction;
import environment.Time;
import environment.Time.Season;
import environment.HabitatNetwork;
import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;

/**
 * @author Patrick.Lambert
 * 
 */

@ServiceProvider(service = AquaNismsGroupProcess.class)
public class MigrateToRiverGrowth extends
AquaNismsGroupProcess<DiadromousFish, DiadromousFishGroup> {

	private double ageThresholdForRiverMigration = 0.25; // in year
	private Season migrationToRiverGrowthSeason = Season.AUTOMN;

	@Override
	public void doProcess(DiadromousFishGroup group) {

		if (Time.getSeason(group.getPilot()) ==  migrationToRiverGrowthSeason){
			for (DiadromousFish fish : group.getAquaNismsList()) {
				if (fish.getPosition().getFunction() == EcologicalFunction.SPAWNING &
						fish.getAge() >= ageThresholdForRiverMigration 
						& fish.getStage() == Stage.JUVENILE) {

					Habitat destination = HabitatNetwork.getNextDownstreamFunctionalHabitat(fish.getPosition(), 
							EcologicalFunction.RIVER_GROWTH);
					fish.moveTo(group.getPilot(), destination, group);
				}
			}
		}
	}
	
	public static void main(String[] args) { System.out.println((new
			XStream(new DomDriver())) .toXML(new MigrateToRiverGrowth())); }
}
