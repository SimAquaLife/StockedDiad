/**
 * 
 */
package species;

import org.openide.util.lookup.ServiceProvider;

import species.DiadromousFish.Stage;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import environment.Habitat;
import environment.Habitat.EcologicalFunction;
import environment.Time;
import environment.Time.Season;
import environment.HabitatNetwork;
import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;

/**
 * @author Patrick.Lambert
 * 
 */

@ServiceProvider(service = AquaNismsGroupProcess.class)
public class MigrateBackToOcean extends
AquaNismsGroupProcess<DiadromousFish, DiadromousFishGroup> {

	private Season backToOceanMigrationSeason = Season.SUMMER;

	@Override
	public void doProcess(DiadromousFishGroup group) {

		if (Time.getSeason(group.getPilot()) == backToOceanMigrationSeason ) {
			for (DiadromousFish fish : group.getAquaNismsList()) {
				if (fish.getPosition().getFunction() == EcologicalFunction. SPAWNING
						& fish.getStage() == Stage.MATURE) {

					Habitat destination = HabitatNetwork.getNextDownstreamFunctionalHabitat(fish.getPosition(), 
							EcologicalFunction.OCEAN_GROWTH);
					fish.moveTo(pilot, destination, group);
				}
			}
		}
	}

	public static void main(String[] args) { System.out.println((new
			XStream(new DomDriver())) .toXML(new MigrateBackToOcean())); }

}
