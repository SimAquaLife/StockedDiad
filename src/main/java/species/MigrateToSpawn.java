/**
 * 
 */
package species;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import miscellaneous.Miscellaneous;

import org.openide.util.lookup.ServiceProvider;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import environment.Habitat;
import environment.HabitatNetwork;
import environment.Time;
import environment.Habitat.EcologicalFunction;
import environment.Time.Season;
import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;

/**
 * @author Patrick.Lambert
 * 
 */

@ServiceProvider(service = AquaNismsGroupProcess.class)
public class MigrateToSpawn extends
AquaNismsGroupProcess<DiadromousFish, DiadromousFishGroup> {

	private Season reproductionMigrationSeason = Season.SPRING;
	private double probabilityToBirthHabitat = 0.95;
	private double probabilityToBirthRiver = 0.80;
	@Override
	public void doProcess(DiadromousFishGroup group) {
		if (Time.getSeason(group.getPilot()) == reproductionMigrationSeason){
			long amount,  amountToMove, nb ;
			int nbPossibleDestinations;
			double proba=0.;
			HabitatNetwork hn = group.getEnvironment();

			Map<Habitat,Long> amountToMoveInDestination = new HashMap<Habitat, Long>();

			List<DiadromousFish> fishToBeAdded = new ArrayList<DiadromousFish>();

			for (DiadromousFish fish : group.getAquaNismsList()){
				if (fish.isReadyToSpawn() 
						& fish.getPosition().getFunction()== EcologicalFunction.OCEAN_GROWTH) {

					amountToMoveInDestination.clear();

					// move to the birthplace
					amount = fish.getAmount();
					amountToMove = Miscellaneous.binomialForSuperIndividual(group.getPilot(), amount,
							probabilityToBirthHabitat);
					if (amountToMove > 0) {
						amountToMoveInDestination.put(fish.getBirthHabitat(), amountToMove);
						amount -=amountToMove;
					}

					// move to the river (including birthplace)
					if (amount > 0) {
						amountToMove = Miscellaneous.binomialForSuperIndividual(group.getPilot(), amount,
								probabilityToBirthRiver);
						if (amountToMove >0){
							amount -= amountToMove; // update the remaining fish 
							nb=amountToMove;
							// share the amountToBirthRiver between different destination as a multinomial 
							nbPossibleDestinations = fish.getBirthHabitat().getSimilars().size();
							
							for (Habitat destination : fish.getBirthHabitat().getSimilars()){
								proba = 1. /nbPossibleDestinations;
								amountToMove = Miscellaneous.binomialForSuperIndividual(group.getPilot(), nb, proba);
								if (amountToMove > 0){
									if (amountToMoveInDestination.containsKey(destination)){
										amountToMoveInDestination.put(destination, amountToMoveInDestination.get(destination)+amountToMove);
									}
									else {
										amountToMoveInDestination.put(destination, amountToMove);
									}
								}

								nb -= amountToMove;
								nbPossibleDestinations --;
							}
						}
					}

					// the remaining fish  move randomly in all the spawning grounds
					if (amount > 0) {
						nb=amount;
						// share the amountToBirthRiver between different destination as a multinomial 
						nbPossibleDestinations = hn.getSpwaningGrounds().size();
						for (Habitat destination :  hn.getSpwaningGrounds()){
							proba = 1. /nbPossibleDestinations;
							amountToMove = Miscellaneous.binomialForSuperIndividual(group.getPilot(), nb, proba);
							if (amountToMove > 0){
								if (amountToMoveInDestination.containsKey(destination)){
									amountToMoveInDestination.put(destination, amountToMoveInDestination.get(destination)+amountToMove);
								}
								else {
									amountToMoveInDestination.put(destination, amountToMove);
								}
							}

							nb -= amountToMove;
							nbPossibleDestinations --;
						}
					}
					
					//System.out.print(fish.toString());
					//System.out.println("  "+amountToMoveInDestination.toString());
					
					// udpate the current fish
					Habitat firstDestination = (Habitat)  amountToMoveInDestination.keySet().toArray()[0];
					fish.setAmount(amountToMoveInDestination.get(firstDestination));
					fish.moveTo(group.getPilot(), firstDestination, group);
					
					//and create new fish
					amountToMoveInDestination.remove(firstDestination);
					for (Habitat destination : amountToMoveInDestination.keySet()){
						fishToBeAdded.add(fish.cloneWithNewPositionAndAmount(
								destination, amountToMoveInDestination.get(destination)));
					}
				}
			}

			if (! fishToBeAdded.isEmpty()){
				for (DiadromousFish fish : fishToBeAdded){
					group.addDifferentAquaNism(fish);
				}
			}
		}
	}



	public static void main(String[] args) { System.out.println((new
			XStream(new DomDriver())) .toXML(new MigrateToSpawn())); }
}
