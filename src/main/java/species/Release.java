package species;

import species.DiadromousFish.Stage;
import environment.Time.Season;

public class Release {
	
		private String location;
		private Stage stage;
		private long number;
		private int releaseYear;

		private Release(String location, Stage stage, long number, int releaseYear) {
			this.location = location;
			this.stage = stage;
			this.number = number;
			this.releaseYear = releaseYear;
		}

		public String getLocation() {
			return location;
		}

		public Stage getStage() {
			return stage;
		}

		public long getNumber() {
			return number;
		}

		public int getReleaseYear() {
			return releaseYear;
		}

		public void setReleaseYear(int releaseYear) {
			this.releaseYear = releaseYear;
		}

		public void setNumber(long number) {
			this.number = number;
		}

		public double getAge() {
			switch (stage){
			case LARVAE :
				return 0.;
			case JUVENILE :
				return 0.25;
			default :
				return Double.NaN;
			}
		}

		public double getLength() {
			switch (stage){
			case LARVAE :
				return 5.;
			case JUVENILE :
				return 10.;
			default :
				return Double.NaN;
			}
		}
		

		public Season getSeason() {
			switch (stage){
			case LARVAE :
				return Season.SUMMER;
			case JUVENILE :
				return Season.AUTOMN;
			default :
				return null;
			}
	

	}

}
