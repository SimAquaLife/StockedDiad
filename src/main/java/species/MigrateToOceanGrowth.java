/**
 * 
 */
package species;

import java.util.ArrayList;
import java.util.List;

import miscellaneous.Miscellaneous;

import org.openide.util.lookup.ServiceProvider;

import species.DiadromousFish.Stage;
import umontreal.iro.lecuyer.probdist.Distribution;
import umontreal.iro.lecuyer.probdist.GammaDist;
import umontreal.iro.lecuyer.probdist.UniformDist;
import umontreal.iro.lecuyer.randvar.UniformGen;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import environment.Habitat;
import environment.Habitat.EcologicalFunction;
import environment.HabitatNetwork;
import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;
import fr.cemagref.simaqualife.kernel.util.TransientParameters.InitTransientParameters;
import fr.cemagref.simaqualife.pilot.Pilot;

/**
 * @author Patrick.Lambert
 * 
 */

@ServiceProvider(service = AquaNismsGroupProcess.class)
public class MigrateToOceanGrowth extends
AquaNismsGroupProcess<DiadromousFish, DiadromousFishGroup> {

	private double meanAgeAtOceanMigration = 5.; // in year
	private double sdAgeAtOceanMigration = 1.;
	private boolean moveWithSplit = false;
	private boolean moveWithMerge = true;

	private transient Distribution oceanMigrationDist;
	private transient UniformGen genAlea;

	public static void main(String[] args) { System.out.println((new
			XStream(new DomDriver())) .toXML(new MigrateToOceanGrowth())); }

	@Override
	@InitTransientParameters
	public void initTransientParameters(Pilot pilot) {
		double alpha = Math.pow( meanAgeAtOceanMigration
				/ sdAgeAtOceanMigration, 2);
		double lambda = meanAgeAtOceanMigration
				/ Math.pow(sdAgeAtOceanMigration, 2);
		oceanMigrationDist = new GammaDist(alpha, lambda);

		if (! moveWithSplit){
			genAlea = new UniformGen(pilot.getRandomStream(),
					new UniformDist(0.,1.));
		}
	}

	@Override
	public void doProcess(DiadromousFishGroup group) {

		double probaToMigrate = 0.;
		long amountToMigrate, amountToStay;
		List<DiadromousFish> fishToBeAdded = new ArrayList<DiadromousFish>();
		List<DiadromousFish> fishToBeKilled = new ArrayList<DiadromousFish>();
		boolean toBeKilled;

		for (DiadromousFish fish : group.getAquaNismsList()) {
			if (fish.getPosition().getFunction() == EcologicalFunction.ESTUARINE_GROWTH
					& fish.getStage() == Stage.JUVENILE) {

				// compute the prob to migrate between age and age-delta knowing that it did not migrate before
				probaToMigrate = (oceanMigrationDist.cdf(fish.getAge()) - oceanMigrationDist.cdf(fish.getAge() - environment.Time.getSeasonDuration()))
						/ (1 - oceanMigrationDist.cdf(fish.getAge() - environment.Time.getSeasonDuration()));


				// compute with a probabilistic approach based on superIndividual regardless of fish amount
				if (moveWithSplit){
					// compute with actual fish amount
					amountToMigrate = Miscellaneous.binomialForSuperIndividual(group.getPilot(), fish.getAmount(), probaToMigrate);
					amountToStay= fish.getAmount() - amountToMigrate;

					if (amountToMigrate != 0){
						Habitat destination = HabitatNetwork.getNextDownstreamFunctionalHabitat(fish.getPosition(), 
								EcologicalFunction.OCEAN_GROWTH);
						if (amountToStay == 0){
							fish.setStage(Stage.SUBADULT);
							if (moveWithMerge){
								toBeKilled = fish.moveToWithMerge(group.getPilot(), destination, group);
								if (toBeKilled){
									fishToBeKilled.add(fish);
								}
							}
							else
								fish.moveTo(group.getPilot(), destination, group);
						}
						else {
							DiadromousFish newFish= fish.cloneWithNewPositionAndAmount(destination, amountToMigrate);
							newFish.setStage(Stage.SUBADULT);
							fishToBeAdded.add(newFish);
							fish.setAmount(amountToStay);	
						}
					}
				}
				else {
					// compute with a probabilistic approach based on superIndividual regardless of fish amount
					Habitat destination = HabitatNetwork.getNextDownstreamFunctionalHabitat(fish.getPosition(), 
							EcologicalFunction.OCEAN_GROWTH);
					if (genAlea.nextDouble() < probaToMigrate) {
						fish.setStage(Stage.SUBADULT);

						if (moveWithMerge){
							toBeKilled = fish.moveToWithMerge(group.getPilot(), destination, group);
							if (toBeKilled){
								fishToBeKilled.add(fish);
							}
						}
						else
							fish.moveTo(group.getPilot(), destination, group);
					}
				}
			}
		}

		if (! fishToBeKilled.isEmpty()){
			for (DiadromousFish fish : fishToBeKilled){
				group.addAquaNism(fish);
			}
		}
		if (! fishToBeAdded.isEmpty()){
			for (DiadromousFish fish : fishToBeAdded){
				group.addAquaNism(fish);
			}
		}
	}
}
