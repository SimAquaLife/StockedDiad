package species;

import java.util.ArrayList;
import java.util.List;

import environment.Time;
import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;
import miscellaneous.Miscellaneous;

import org.openide.util.lookup.ServiceProvider;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

@ServiceProvider(service = AquaNismsGroupProcess.class)
public class Die extends AquaNismsGroupProcess<DiadromousFish, DiadromousFishGroup> {

	private double M0_3months =3.6841;
	private double cvM0_3months = 0.2;
	private double M3_12months = 1.8420;
	private double cvM3_12months = 0.;
	private double M1year = 1.2040;
	private double cvM1year = 0.;
	private double Mafter = 0.5300;
	private double cvMafter = 0.;

	@Override
	public void doProcess(DiadromousFishGroup group) {
		List<DiadromousFish> deadFish = new ArrayList<DiadromousFish>();
		double survival; 
		long survivalAmount;
		double M0_3monthsAlea = Miscellaneous.logNormalAlea(group.getPilot(), M0_3months, cvM0_3months);	
		double M3_12monthsAlea = Miscellaneous.logNormalAlea(group.getPilot(), M3_12months, cvM3_12months);
		double M1yearAlea = Miscellaneous.logNormalAlea(group.getPilot(), M1year, cvM1year);
		double MafterAlea = Miscellaneous.logNormalAlea(group.getPilot(), Mafter, cvMafter);
		for(DiadromousFish fish : group.getAquaNismsList()){
			if (fish.getAge() <.25)
				survival = Math.exp(-M0_3monthsAlea * Time.getSeasonDuration());
			else if (fish.getAge()<1)
				survival =Math.exp(-M3_12monthsAlea * Time.getSeasonDuration());
			else if (fish.getAge() <2)
				survival = Math.exp(-M1yearAlea * Time.getSeasonDuration());
			else
				survival = Math.exp(-MafterAlea * Time.getSeasonDuration());

			survivalAmount = Miscellaneous.binomialForSuperIndividual(group.getPilot(), fish.getAmount(), survival);

			if (survivalAmount == 0)
				deadFish.add(fish);
			else
				fish.setAmount(survivalAmount);
		}

		//long deadAmount=0;
		for (DiadromousFish fish : deadFish){
			//deadAmount += fish.getAmount();
			group.removeAquaNism(fish);
		}
	}


	public static void main(String[] args) { System.out.println((new
			XStream(new DomDriver())) .toXML(new Die())); }
}
