package species;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import miscellaneous.QueueMemory;
import fr.cemagref.observation.kernel.Observable;
import fr.cemagref.observation.kernel.ObservablesHandler;
import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;
import fr.cemagref.simaqualife.kernel.util.TransientParameters.InitTransientParameters;
import fr.cemagref.simaqualife.pilot.Pilot;

public class SurveyByScientist extends AquaNismsGroupProcess<DiadromousFish, DiadromousFishGroup> {

	private int memorySize = 10;
	

	private transient QueueMemory<Double> fishEffectives;
	protected static transient ObservablesHandler cobservable;
	
	@Observable(description = "geometric mean of fish Number")
	public double getGeometricMeanFisnNumber(){
		return Math.round(fishEffectives.getGeometricMean()*100.)/10.;
	}
	
	@Override
	@InitTransientParameters
	public void initTransientParameters(Pilot pilot) {
		super.initTransientParameters(pilot);
		fishEffectives = new QueueMemory<Double>(memorySize);
		
		cobservable = pilot.addObservable(this.getClass());
	}
	
	@Override
	public void doProcess(DiadromousFishGroup group) {
		double fishNumber = group.getFishEffective();
		fishEffectives.push(fishNumber);
		
		this.getCobservable().fireChanges(this, pilot.getCurrentTime());
		

	}
	
	public ObservablesHandler getCobservable() {
		return cobservable;
	}
	
	public static void main(String[] args) { System.out.println((new
			XStream(new DomDriver())) .toXML(new SurveyByScientist())); }
}
