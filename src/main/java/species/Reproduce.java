package species;

import java.util.List;

import miscellaneous.Miscellaneous;
import miscellaneous.QueueMemory;

import org.openide.util.lookup.ServiceProvider;

import species.DiadromousFish.Gender;
import species.DiadromousFish.Origin;
import species.DiadromousFish.Stage;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import environment.Habitat;
import environment.Time;
import environment.Time.Season;
import fr.cemagref.observation.kernel.Observable;
import fr.cemagref.observation.kernel.ObservablesHandler;
import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;
import fr.cemagref.simaqualife.kernel.util.TransientParameters.InitTransientParameters;
import fr.cemagref.simaqualife.pilot.Pilot;

@ServiceProvider(service = AquaNismsGroupProcess.class)
public class Reproduce extends AquaNismsGroupProcess<DiadromousFish, DiadromousFishGroup>{

	private Season reproductionSeason = Season.SPRING;
	private double maximumRecruitmentDensity = 400.; //nb of recruit /m2 from 500 m2 for 1 fem in Jego et al 
	private double fecundity =200000.;  // nb or recruit per female
	private long alleeOffset =0; // minimum number of female to have a reproduction
	private double proportionOfFemale =.5;
	private double cvRecruitment = .2;
	private int memorySize = 10;
	private int femaleThreshold = 5;
	private int stopCriterion = 20;
	
	private boolean addDifferent = true;
	
	@Observable(description = "time of last female number fail")
	private transient double timeOfLastFemaleNumberFail;
	
	@Observable(description = "number of reproduction")
	private transient int reproductionNumber;
	
	@Observable(description = "nb of active female spawners")
	private transient double nbActiveFemaleSpawner;
	
	@Observable(description = "nb of wild active female spawners")
	private transient double nbWildActiveFemaleSpawner;
	
	@Observable(description = "nb of active male spawners")
	private transient double nbActiveMaleSpawner;

	@Observable(description = "nb of recruits")
	private transient double nbTotalRecruit;

	@Observable (description = "nb of SI per reproduction")
	private transient double nbSIPerRepro; 

	private transient long amountInSuperIndividual;
	protected static transient ObservablesHandler cobservable;
	
	//private transient NormalGen genNormal;
	private transient QueueMemory<Double> wildActiveFemaleSpawnerNumbers;
	
	@Observable(description = "mean nb of Wild active female fpawner Number")
	public double getMeanLastWildActiveFemaleSpawnerNumber(){
		return Math.round(wildActiveFemaleSpawnerNumbers.getMean() * 100.)/100.;
	}

	private transient Long timeStepStop;
	
	public static void main(String[] args) { System.out.println((new
			XStream(new DomDriver())) .toXML(new Reproduce())); }

	@Override
	@InitTransientParameters
	public void initTransientParameters(Pilot pilot) {
		super.initTransientParameters(pilot);
		amountInSuperIndividual = -1;

		//genNormal = new NormalGen( Pilot.getRandomStream(),
		//		new NormalDist(0., 1.));

		timeStepStop = pilot.getSimDuration();
		wildActiveFemaleSpawnerNumbers = new QueueMemory<Double>(memorySize);
		
		reproductionNumber = 0;
		cobservable = pilot.addObservable(this.getClass());
	}

	@Override
	public void doProcess(DiadromousFishGroup group) {

		if (Time.getSeason(group.getPilot()) == reproductionSeason){	

			if (amountInSuperIndividual == -1){
				if (group.getMeanSurvival() == 0.)
					amountInSuperIndividual= group.getDefaultAmountInSuperIndividual();
				else
					amountInSuperIndividual =  (long) (1./(group.getMeanLifespan() * group.getMeanSurvival()));
			}

			nbActiveFemaleSpawner = 0;
			nbWildActiveFemaleSpawner=0.;
			nbActiveMaleSpawner = 0;
			nbTotalRecruit = 0;
			nbSIPerRepro = 0.;

			double numberOfFemales, numberOfMales, numberOfWildFemales;
			long numberOfRecruit;

			// list the spawning grounds
			for(Habitat spawningGround : group.getEnvironment().getSpwaningGrounds()){

				numberOfFemales = 0.;
				numberOfWildFemales = 0.;
				numberOfMales = 0.;
				numberOfRecruit = 0;

				// list the spawners present on spawning ground
				List<DiadromousFish> fishes =  spawningGround.getFishes();
				if (fishes != null){
					for( DiadromousFish fish : fishes){
						if (fish .isReadyToSpawn()){
							if (fish.getGender() == Gender.FEMALE) {
								numberOfFemales += fish.getAmount();
								if (fish.getOrigin() == Origin.WILD)
									numberOfWildFemales +=fish.getAmount();
							}
							else
								numberOfMales += fish.getAmount();

							fish.incrementNumberOfReproduction();	
							fish.setReadyToSpawn(false);
							fish.setAgeAtLastReproduction(fish.getAge());
						}
					}
				}			

				if (numberOfFemales > alleeOffset & numberOfMales > 0.) {

					// Reproduction process = hockey-stick Stock-Recruitment relationship Allee offset
					if (maximumRecruitmentDensity ==0)
						numberOfRecruit = 0;
					else{
						double meanNumberOfRecruit = Math.round(Math.max(0,
								maximumRecruitmentDensity * fecundity * (numberOfFemales-alleeOffset)
								/(maximumRecruitmentDensity - fecundity * alleeOffset)));
						numberOfRecruit = Math.round(Miscellaneous.logNormalAlea(group.getPilot(), meanNumberOfRecruit, cvRecruitment));
				
						// mortality induced by the quality of the habitat
						numberOfRecruit *= spawningGround.getHabitatEmbryonicSurvival();
						
					}
					// Creation of new superFish
					if(numberOfRecruit > 0){
						// true active spawners (really produce offsprings)
						reproductionNumber ++;
						nbActiveFemaleSpawner += numberOfFemales -alleeOffset;
						nbWildActiveFemaleSpawner += Math.round(numberOfWildFemales*(1-alleeOffset/numberOfFemales));
						nbActiveMaleSpawner += numberOfMales;


						long numberOfFemaleRecruit = (long) (numberOfRecruit * proportionOfFemale);
						long numberOfMaleRecruit = numberOfRecruit - numberOfFemaleRecruit;

						// female
						int numberOfsuperIndividual = (int)	Math.floor((double) numberOfFemaleRecruit / (double) amountInSuperIndividual);
						for (int i=0; i<numberOfsuperIndividual; i++){
							if (addDifferent)

								group.addDifferentAquaNism(new DiadromousFish(group.getPilot(), spawningGround, Stage.LARVAE, Gender.FEMALE, amountInSuperIndividual, 0., 5., Origin.WILD));
							else
								group.addAquaNism(new DiadromousFish(group.getPilot(), spawningGround, Stage.LARVAE, Gender.FEMALE, amountInSuperIndividual, 0., 5., Origin.WILD));
						}
						nbSIPerRepro += numberOfsuperIndividual;

						if (amountInSuperIndividual * numberOfsuperIndividual <numberOfFemaleRecruit  ) {
							if (addDifferent)
								group.addDifferentAquaNism(new DiadromousFish(group.getPilot(), spawningGround, Stage.LARVAE, Gender.FEMALE,
										numberOfFemaleRecruit - amountInSuperIndividual * numberOfsuperIndividual , 0., 5., Origin.WILD));
							else
								group.addAquaNism(new DiadromousFish(group.getPilot(), spawningGround, Stage.LARVAE, Gender.FEMALE,
										numberOfFemaleRecruit - amountInSuperIndividual * numberOfsuperIndividual , 0., 5., Origin.WILD));

							nbSIPerRepro ++;
						}

						// male
						numberOfsuperIndividual = (int) Math.floor((double) numberOfMaleRecruit / (double) amountInSuperIndividual);
						for (int i=0; i<numberOfsuperIndividual; i++){
							if (addDifferent)
								group.addDifferentAquaNism(new DiadromousFish(group.getPilot(), spawningGround, Stage.LARVAE, Gender.MALE, amountInSuperIndividual, 0., 5., Origin.WILD));
							else
								group.addDifferentAquaNism(new DiadromousFish(group.getPilot(), spawningGround, Stage.LARVAE, Gender.MALE, amountInSuperIndividual, 0., 5., Origin.WILD));	
						}
						nbSIPerRepro += numberOfsuperIndividual;
						if (amountInSuperIndividual * numberOfsuperIndividual <numberOfMaleRecruit  ) {
							if (addDifferent)
								group.addDifferentAquaNism(new DiadromousFish(group.getPilot(), spawningGround, Stage.LARVAE, Gender.MALE,
										numberOfMaleRecruit - amountInSuperIndividual * numberOfsuperIndividual , 0., 5., Origin.WILD));
							else
								group.addAquaNism(new DiadromousFish(group.getPilot(), spawningGround, Stage.LARVAE, Gender.MALE,
										numberOfMaleRecruit - amountInSuperIndividual * numberOfsuperIndividual , 0., 5., Origin.WILD));
							nbSIPerRepro ++;
						}
					}
				}

				nbTotalRecruit += numberOfRecruit;
			}

			// store the last active female spawner
			wildActiveFemaleSpawnerNumbers.push(nbWildActiveFemaleSpawner);
			if (nbWildActiveFemaleSpawner < femaleThreshold)
				timeOfLastFemaleNumberFail= pilot.getCurrentTime() * Time.getSeasonDuration();		
			
			if (wildActiveFemaleSpawnerNumbers.getMean() > stopCriterion){
				timeStepStop = pilot.getCurrentTime();
				pilot.stop();
			}
			
				
				this.getCobservable().fireChanges(this, pilot.getCurrentTime());
			//System.out.println(nbActiveFemaleSpawner + " / " + nbActiveMaleSpawner + " --> " + nbTotalRecruit);
		}
	}
	
	public double getNbWildActiveFemaleSpawner() {
		return nbWildActiveFemaleSpawner;
	}

	public double getTimeOfLastFemaleNumberFail() {
		return timeOfLastFemaleNumberFail;
	}

	public ObservablesHandler getCobservable() {
		return cobservable;
	}

	public Long getTimeStepStop() {
		return timeStepStop;
	}

	public int getReproductionNumber() {
		return reproductionNumber;
	}

	
	
	
}


