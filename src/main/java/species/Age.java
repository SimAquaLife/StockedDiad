package species;

import environment.Time;
import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;

import org.openide.util.lookup.ServiceProvider;

import species.DiadromousFish.Stage;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

@ServiceProvider(service = AquaNismsGroupProcess.class)
public class Age extends AquaNismsGroupProcess<DiadromousFish, DiadromousFishGroup> {

	public static void main(String[] args) { System.out.println((new
			XStream(new DomDriver())) .toXML(new Age())); }
	
	@Override
	public void doProcess(DiadromousFishGroup group) {
		for(DiadromousFish fish : group.getAquaNismsList()){
			//Age
			fish.incrementAge( Time.getSeasonDuration());
			
			if (fish.getStage() == Stage.LARVAE & fish.getAge() >=.25)
				fish.setStage(Stage.JUVENILE);
				
		}
	}
}
