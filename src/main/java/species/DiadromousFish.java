package species;

import java.lang.reflect.Field;

import environment.Habitat;
import environment.HabitatNetwork;
import fr.cemagref.simaqualife.kernel.AquaNism;
import fr.cemagref.simaqualife.kernel.AquaNismsGroup;
import fr.cemagref.simaqualife.pilot.Pilot;

public class DiadromousFish extends AquaNism<Habitat, HabitatNetwork> implements Cloneable {

	public static enum Stage {LARVAE, JUVENILE, SUBADULT, MATURE};
	public static enum Gender {FEMALE, MALE}; 
	public static enum Origin {WILD, STOCKED};

	private final Habitat birthHabitat;
	private final Gender gender; 
	private long amount;
	private double age;
	private double length;
	private Stage stage;
	private int numberOfReproduction;
	private final Origin origin;
	private double ageAtLastReproduction;
	private boolean readyToSpawn;

	public DiadromousFish(Pilot pilot, Habitat birthHabitat, Stage stage, Gender gender,
			long amount, double age, double length, Origin origin) {
		super(pilot, birthHabitat);
		this.birthHabitat = birthHabitat;
		this.gender = gender;
		this.amount = amount;
		this.age = age;
		this.length = length;
		this.origin = origin;
		this.stage = stage;
		this.numberOfReproduction = 0;
		this.ageAtLastReproduction = Double.NaN;
		this.readyToSpawn = false;
	}

	@Override
	public <ANG extends AquaNismsGroup<?, HabitatNetwork>> void moveTo(Pilot pilot,
			Habitat destination, ANG group) {

		if (this.position != destination) {
			this.position.removeFish(this);
			destination.addFish(this);
		}
		super.moveTo(pilot, destination, group);
	}

	public  boolean moveToWithMerge(Pilot pilot, Habitat destination, DiadromousFishGroup group) {
		boolean toBeKilled = false;
		if (this.position != destination) {
			// searh for a fish with same characteristics except amount in the destination habitat
			DiadromousFish similarFish = null;
			if (destination.getFishes() != null){
				for (DiadromousFish otherFish : destination.getFishes()){
					if (otherFish.isSimilar(this)){
						similarFish = otherFish;
						break;
					}
				}
			}

			//move if there is no similar fish or update the amount in the similar fish
			if (similarFish == null){
				this.position.removeFish(this);
				this.position = destination;
				destination.addFish(this);

				if (cobservable != null) {
					cobservable.fireChanges(this, pilot.getCurrentTime());
				}
					}
			else {
				similarFish.incrementAmount(this.getAmount());
				this.setAmount(0);
				toBeKilled = true;
			}
		}
		return toBeKilled;
				
	}


	public long getAmount() {
		return amount;
	}

	public void setAmount(long amount) {
		this.amount = amount;
	}

	public double getAge() {
		return age;
	}

	public void incrementAge(double duration) {
		this.age += duration;
	}

	public double getLength() {
		return length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	public int getNumberOfReproduction() {
		return numberOfReproduction;
	}

	public void incrementNumberOfReproduction() {
		this.numberOfReproduction += 1;
	}

	public Habitat getBirthHabitat() {
		return birthHabitat;
	}

	public Gender getGender() {
		return gender;
	}

	public Origin getOrigin() {
		return origin;
	}

	private void setPosition(Habitat newPosition){
		this.position = newPosition;
	}

	public double getAgeAtLastReproduction() {
		return ageAtLastReproduction;
	}

	public void setAgeAtLastReproduction(double ageAtLastReproduction) {
		this.ageAtLastReproduction = ageAtLastReproduction;
	}

	public boolean isReadyToSpawn() {
		return readyToSpawn;
	}

	public void setReadyToSpawn(boolean readyToSpawn) {
		this.readyToSpawn = readyToSpawn;
	}

	public DiadromousFish cloneWithNewPositionAndAmount(Habitat newPosition, long newAmount){
		DiadromousFish newFish = null;
		try {
			newFish = (DiadromousFish) this.clone();
			newFish.setPosition(newPosition);
			newFish.setAmount(newAmount);
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return newFish;
	}

	public boolean isSimilar(DiadromousFish fish){
		//bring the fields except amount and compare the values between the two instances
		boolean test = true;
		Field[] fields = DiadromousFish.class.getDeclaredFields();
		for (Field field : fields){
			if (field.getName() != "amount"){
				field.setAccessible(true);
				try {				
					if (!field.get(fish).equals(field.get(this))){
						test = false;
						break;
					}
				} catch (IllegalArgumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return test;
	}

	public void incrementAmount (long amount){
		this.amount += amount;
	}

	@Override
	public String toString() {
		return "DiadromousFish [birthHabitat=" + birthHabitat + ", gender="
				+ gender + ", amount=" + amount + ", age=" + age + ", stage="
				+ stage + ", origin=" + origin + ", readyToSpawn="
				+ readyToSpawn + "]";
	}
	

}
