package species;

import java.lang.reflect.InvocationTargetException;

import species.DiadromousFish.Gender;
import species.DiadromousFish.Stage;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import environment.Habitat.EcologicalFunction;
import environment.HabitatNetwork;
import fr.cemagref.observation.kernel.Observable;
import fr.cemagref.simaqualife.kernel.AquaNismsGroup;
import fr.cemagref.simaqualife.kernel.util.TransientParameters.InitTransientParameters;
import fr.cemagref.simaqualife.pilot.Pilot;

public class DiadromousFishGroup extends AquaNismsGroup<DiadromousFish, HabitatNetwork> {

	// to calibrate the amount of fish in super-individual
	private long defaultAmountInSuperIndividual;
	private transient double meanLifespan;
	private  transient double meanSurvival;

	public static void main(String[] args) { System.out.println((new
			XStream(new DomDriver())) .toXML(new DiadromousFishGroup()));
	}

	@Override
	@InitTransientParameters
	public void initTransientParameters(Pilot pilot) throws IllegalArgumentException,
	IllegalAccessException, InvocationTargetException {
		super.initTransientParameters(pilot);
		meanSurvival = 0.;
		meanLifespan = 0.;		;
	}

	public long getDefaultAmountInSuperIndividual() {
		return defaultAmountInSuperIndividual;
	}

	public double getMeanSurvival() {
		return meanSurvival;
	}

	public void setMeanSurvival(double meansurvival) {
		this.meanSurvival = meansurvival;
	}

	public void setMeanLifespan(double meanLifespan) {
		this.meanLifespan = meanLifespan;
	}

	@Override
	public void addAquaNism(DiadromousFish fish) {
		// TODO Auto-generated method stub
		super.addAquaNism(fish);
		fish.getPosition().addFish(fish);
	}

	public void addDifferentAquaNism(DiadromousFish fish) {

		// searh for a fish with same characteristics except amount
		DiadromousFish similarFish = null;
		if (fish.getPosition().getFishes() != null){
			for (DiadromousFish otherFish : fish.getPosition().getFishes()){
				if (otherFish.isSimilar(fish)){
					similarFish = otherFish;
					break;
				}
			}
		}

	// add a new fish if there is no similar fish or update the amount in the similar fish
	if (similarFish == null){
		super.addAquaNism(fish);
		fish.getPosition().addFish(fish);
	}
	else {
		similarFish.incrementAmount(fish.getAmount());
	}
}

@Override
public void removeAquaNism(DiadromousFish fish) {
	// TODO Auto-generated method stub
	super.removeAquaNism(fish);
	fish.getPosition().removeFish(fish);
}

@Observable(description = "Nb of SI")
public int getNbSI() {
	return this.getAquaNismsList().size();
}

@Observable(description = "Nb of fish")
public double getFishEffective() {
	long eff = 0;
	for(DiadromousFish fish : this.getAquaNismsList()){
		eff += fish.getAmount();
	}
	return eff;
}

@Observable(description = "Nb of female")
public double getFemaleEffective() {
	long eff = 0;
	for(DiadromousFish fish : this.getAquaNismsList()){
		if (fish.getGender() == Gender.FEMALE)
			eff += fish.getAmount();
	}
	return eff;
}

@Observable(description = "Nb of female spawner in ocean")
public double getFemaleSpawnerInOceanEffective() {
	long eff = 0;
	for(DiadromousFish fish : this.getAquaNismsList()){
		if (fish.getGender() == Gender.FEMALE & fish.getPosition().getFunction() == EcologicalFunction.OCEAN_GROWTH & fish.getStage() == Stage.MATURE)
			eff += fish.getAmount();
	}
	return eff;
}

@Observable(description = "Nb of male spawner in ocean")
public double getMaleSpawnerInOceanEffective() {
	long eff = 0;
	for(DiadromousFish fish : this.getAquaNismsList()){
		if (fish.getGender() == Gender.MALE & fish.getPosition().getFunction() == EcologicalFunction.OCEAN_GROWTH & fish.getStage() == Stage.MATURE)
			eff += fish.getAmount();
	}
	return eff;
}

public double getMeanLifespan() {
	return meanLifespan;
}
}
