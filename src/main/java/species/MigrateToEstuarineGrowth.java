/**
 * 
 */
package species;

import java.util.ArrayList;
import java.util.List;

import miscellaneous.Miscellaneous;

import org.openide.util.lookup.ServiceProvider;

import species.DiadromousFish.Stage;
import umontreal.iro.lecuyer.probdist.Distribution;
import umontreal.iro.lecuyer.probdist.GammaDist;
import umontreal.iro.lecuyer.probdist.UniformDist;
import umontreal.iro.lecuyer.randvar.UniformGen;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

import environment.Habitat;
import environment.Habitat.EcologicalFunction;
import environment.HabitatNetwork;
import fr.cemagref.simaqualife.kernel.processes.AquaNismsGroupProcess;
import fr.cemagref.simaqualife.kernel.util.TransientParameters.InitTransientParameters;
import fr.cemagref.simaqualife.pilot.Pilot;

/**
 * @author Patrick.Lambert
 * 
 */

@ServiceProvider(service = AquaNismsGroupProcess.class)
public class MigrateToEstuarineGrowth extends
AquaNismsGroupProcess<DiadromousFish, DiadromousFishGroup> {

	private double meanAgeAtEstuarineMigration = 1.5; // in year
	private double sdAgeAtEstuarineMigration = .2;
	private boolean moveWithSplit = false;
	private boolean moveWithMerge = true;

	private transient Distribution estuarineMigrationDist;
	private transient UniformGen genAlea;

	public static void main(String[] args) { System.out.println((new
			XStream(new DomDriver())).toXML(new MigrateToEstuarineGrowth())); }

	@Override
	@InitTransientParameters
	public void initTransientParameters(Pilot pilot) {
		double alpha = Math.pow( meanAgeAtEstuarineMigration
				/ sdAgeAtEstuarineMigration, 2);
		double lambda = meanAgeAtEstuarineMigration
				/ Math.pow(sdAgeAtEstuarineMigration, 2);
		estuarineMigrationDist = new GammaDist(alpha, lambda);

		if (! moveWithSplit){
			genAlea = new UniformGen(pilot.getRandomStream(),
					new UniformDist(0.,1.));
		}
	}

	@Override
	public void doProcess(DiadromousFishGroup group) {

		double probaToMigrate = 0.;
		long amountToMigrate, amountToStay;
		List<DiadromousFish> fishToBeAdded = new ArrayList<DiadromousFish>();
		List<DiadromousFish> fishToBeKilled = new ArrayList<DiadromousFish>();
		boolean toBeKilled;

		for (DiadromousFish fish : group.getAquaNismsList()) {
			if (fish.getPosition().getFunction() == EcologicalFunction.RIVER_GROWTH
					& fish.getStage() == Stage.JUVENILE) {

				// compute the prob to migrate between age and age-delta knowing that it did not migrate before
				probaToMigrate = (estuarineMigrationDist.cdf(fish.getAge()) - estuarineMigrationDist.cdf(fish.getAge() - environment.Time.getSeasonDuration()))
						/ (1 - estuarineMigrationDist.cdf(fish.getAge() - environment.Time.getSeasonDuration()));
				
				if (moveWithSplit){
					// compute with actual fish amount
					amountToMigrate = Miscellaneous.binomialForSuperIndividual(group.getPilot(), fish.getAmount(), probaToMigrate);
					amountToStay= fish.getAmount() - amountToMigrate;

					if (amountToMigrate != 0){
						Habitat destination = HabitatNetwork.getNextDownstreamFunctionalHabitat(fish.getPosition(), 
								EcologicalFunction.ESTUARINE_GROWTH);

						if (amountToStay == 0){
							if (moveWithMerge){
								toBeKilled=fish.moveToWithMerge(group.getPilot(), destination, group);
								if (toBeKilled){
									fishToBeKilled.add(fish);
								}
							} 
							else{
								fish.moveTo(group.getPilot(), destination, group);
							}
						}
						else {
							DiadromousFish newFish= fish.cloneWithNewPositionAndAmount(destination, amountToMigrate);
							fishToBeAdded.add(newFish);
							fish.setAmount(amountToStay);	
						}
					}
				}
				else {
					// compute with a probabilistic approach based on superIndividual regardless of fish amount
					if (genAlea.nextDouble() < probaToMigrate) {
						Habitat destination = HabitatNetwork.getNextDownstreamFunctionalHabitat(fish.getPosition(), 
								EcologicalFunction.ESTUARINE_GROWTH);
						if (moveWithMerge) {
							toBeKilled = fish.moveToWithMerge(group.getPilot(), destination, group);
							if (toBeKilled){
								fishToBeKilled.add(fish);
							}
						} 
						else {
							fish.moveTo(group.getPilot(), destination, group);
						}
					}
				}
			}
		}

		if (! fishToBeKilled.isEmpty()){
			for (DiadromousFish fish : fishToBeKilled){
				group.addAquaNism(fish);
			}
		}

		if (! fishToBeAdded.isEmpty()){
			for (DiadromousFish fish : fishToBeAdded){
				group.addAquaNism(fish);
			}
		}
	}
}
