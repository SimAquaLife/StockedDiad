
// cd C:\Program Files\OpenMOLE
// openmole.bat -c -p D:\workspace\StockedDiad\target\StockedDiad-0.0.1-SNAPSHOT.jar -s D:\workspace\StockedDiad\openMole\SD4a.scala 2>D:\workspace\StockedDiad\data\output\erreurOpenMole.txt
// 1>&2

// pour un lancement sur le serveur de calcul
// rm -r ~/.openmole/.tmp; 
// /usr/local/openmole/openmole -c -configuration /opt/StockedDiad/ -p /opt/StockedDiad/target/StockedDiad-0.0.1-SNAPSHOT.jar -s /opt/StockedDiad/openMole/SD4a.scala 2>/opt/StockedDiad/data/output/erreurOpenMoleA.txt

import org.openmole.plugin.task.groovy._
import org.openmole.plugin.domain.range._
import org.openmole.plugin.hook.display._
import org.openmole.plugin.environment.pbs._
import org.openmole.core.model.execution.Environment
import org.openmole.plugin.domain.collection._
import org.openmole.plugin.sampling.combine._
import org.openmole.plugin.domain.distribution._
import org.openmole.plugin.hook.display._
import org.openmole.plugin.hook.file._
import org.openmole.plugin.environment.pbs._

// pour une execution sur ma machine
//val wd = "D:/workspace/StockedDiad/"

// pour une xecution sur le serveur
val wd ="/opt/StockedDiad/"


// lancement sur la ferme de calcul de Clermont
//logger.level("FINE")
//val env = PBSEnvironment("rougier", "calcul64.clermont.cemagref.fr")

// ================================= prototype
// ----------- input 
val femaleMeanAgeAtMaturation = Prototype[Double]("femaleMeanAgeAtMaturation")
val femaleSdAgeAtMaturation = Prototype[Double]("femaleSdAgeAtMaturation")
val femaleMeanInterSpawningInterval = Prototype[Double]("femaleMeanInterSpawningInterval")
val maleMeanAgeAtMaturation = Prototype[Double]("maleMeanAgeAtMaturation")
val maleSdAgeAtMaturation = Prototype[Double]("maleSdAgeAtMaturation")
val maleMeanInterSpawningInterval = Prototype[Double]("maleMeanInterSpawningInterval")

val probabilityToBirthRiver = Prototype[Double]("probabilityToBirthRiver")
val probabilityToBirthHabitat = Prototype[Double]("probabilityToBirthHabitat")

val fecundity = Prototype[Double]("fecundity")
val cvRecruitment = Prototype[Double]("cvRecruitment")

val M0_3months = Prototype[Double]("M0_3months")
val cvM0_3months = Prototype[Double]("cvM0_3months")
val M3_12months = Prototype[Double]("M3_12months")
val cvM3_12months = Prototype[Double]("cvM3_12months")
val M1year = Prototype[Double]("M1year")
val cvM1year = Prototype[Double]("cvM1year")
val Mafter = Prototype[Double]("Mafter")
val cvMafter = Prototype[Double]("cvMafter")

val releaseAmount = Prototype[Int]("releaseAmount")

val replicat = Prototype[Int]("replicat")
val groupFile = Prototype[File]("groupFile")
val envFile = Prototype[File]("envFile")
val habitatNetwork =Prototype[File]("habitatNetwork")

// ---------------- output
//val femaleSpawnerInOceanEffective = Prototype[Double]("femaleSpawnerInOceanEffective")
val wildActiveFemaleSpawner = Prototype[Double]("wildActiveFemaleSpawner")
val meanWildActiveFemaleSpawner = Prototype[Double]("meanWildActiveFemaleSpawner")
val lastFailInReproduction = Prototype[Double]("lastFailInReproduction")
val geoMeanEffective = Prototype[Double]("geoMeanEffective")
val timeStepStop = Prototype[Long]("timeStepStop")
val reproductionNumber = Prototype[Integer]("reproductionNumber")

// ======================================== exploration
  
  val explo = ExplorationTask("explo",
	Factor(femaleMeanAgeAtMaturation, List(15.4,17.4) toDomain) x
 	Factor(femaleSdAgeAtMaturation, List(2.2, 3.2) toDomain) x
	Factor(femaleMeanInterSpawningInterval, List(3.5, 4.5) toDomain) x
 	Factor(maleMeanAgeAtMaturation, List(7., 9.) toDomain) x
 	Factor(maleSdAgeAtMaturation, List(1.1, 1.7) toDomain) x
	Factor(maleMeanInterSpawningInterval, List(1.2, 1.5) toDomain) x
	Factor(probabilityToBirthHabitat, List(0., 0.9) toDomain) x
	Factor(probabilityToBirthRiver, List(0., 0.9) toDomain) x
 	Factor(fecundity, List(180000., 220000.) toDomain) x
 	Factor(cvRecruitment, List(0.2, 0.3) toDomain) x
	Factor(M0_3months, List(2.947, 4.420) toDomain) x
	Factor(cvM0_3months, List(0.) toDomain) x	
	Factor(M3_12months, List(1.474, 2.210) toDomain) x
	Factor(cvM3_12months, List(0.)  toDomain) x	
	Factor(M1year, List(0.963, 1.444) toDomain) x
	Factor(cvM1year,  List(0.)  toDomain) x
	Factor(Mafter, List(0.424, 0.636) toDomain) x
	Factor(cvMafter,  List(0.) toDomain) x
	Factor(releaseAmount, List(25000, 100000) toDomain) x
    Factor(replicat, 1 to 5 by 1 toDomain)
	)	
	
// tache Groovy
val model = GroovyTask("model", scala.io.Source.fromFile(wd + "openMole/SD4.groovy").mkString)
model addImport "fr.cemagref.simaqualife.extensions.pilot.BatchRunner"
model addImport "fr.cemagref.simaqualife.pilot.Pilot"
model addImport "miscellaneous.ReflectUtils"
model addInput femaleMeanAgeAtMaturation
model addInput femaleSdAgeAtMaturation
model addInput femaleMeanInterSpawningInterval
model addInput maleMeanAgeAtMaturation
model addInput maleSdAgeAtMaturation
model addInput maleMeanInterSpawningInterval
model addInput probabilityToBirthRiver
model addInput probabilityToBirthHabitat
model addInput fecundity
model addInput cvRecruitment
model addInput M0_3months
model addInput cvM0_3months
model addInput M3_12months
model addInput cvM3_12months
model addInput M1year
model addInput cvM1year
model addInput Mafter
model addInput cvMafter
model addInput releaseAmount
model addInput replicat
model addParameter (groupFile -> new File(wd+"data/input/sturgeonREFERENCE.xml"))
model addParameter (envFile -> new File(wd+"data/input/Gironde.xml"))
model addParameter (habitatNetwork -> new File(wd + "data/input/GirondeSturgeon.csv"))
model addOutput femaleMeanAgeAtMaturation
model addOutput femaleSdAgeAtMaturation
model addOutput femaleMeanInterSpawningInterval
model addOutput maleMeanAgeAtMaturation
model addOutput maleSdAgeAtMaturation
model addOutput maleMeanInterSpawningInterval
model addOutput probabilityToBirthRiver
model addOutput probabilityToBirthHabitat
model addOutput fecundity
model addOutput cvRecruitment
model addOutput M0_3months
model addOutput cvM0_3months
model addOutput M3_12months
model addOutput cvM3_12months
model addOutput M1year
model addOutput cvM1year
model addOutput Mafter
model addOutput cvMafter
model addOutput releaseAmount
model addOutput replicat
//model addOutput femaleSpawnerInOceanEffective
model addOutput wildActiveFemaleSpawner
model addOutput meanWildActiveFemaleSpawner
model addOutput lastFailInReproduction
model addOutput geoMeanEffective
model addOutput timeStepStop
model addOutput reproductionNumber

	// sur l'ordi
	//val h = AppendToCSVFileHook("SAL/stockedDiadOutput4a.txt")

	 sur le serveur de calcul (pas la m�me version d'openMole que sur ma machine)
	val h = new AppendToCSVFileHook(wd + "data/output/stockedDiadOutput4a.txt")


val ex = explo -< (model hook h) toExecution
ex.start
ex.waitUntilEnded