
envFile = newFile();
envFile << "<environment.HabitatNetwork>";
envFile << "<habitatNetworkFileName>" + habitatNetwork.getAbsolutePath() + "</habitatNetworkFileName>";
envFile << "</environment.HabitatNetwork>";

obsFile = newFile();
obsFile << "<hashtable></hashtable>";

arguments = ['-simDuration','68','-simBegin','1','-timeStepDuration','1',
  '-groups', groupFile.getAbsolutePath(),'-env',envFile.getAbsolutePath(),'-observers',obsFile.getAbsolutePath(), 
  '-RNGStatusIndex', replicat
  ] as String[]

path = "aquaticWorld.aquaNismsGroupsList.0.processes.processesEachStep."
  
Pilot.init()

BatchRunner.parseArgs(arguments,false,true,false);

ReflectUtils.setFieldValueFromPath(Pilot.getInstance(), path+"3.femaleMeanAgeAtMaturation", femaleMeanAgeAtMaturation);
ReflectUtils.setFieldValueFromPath(Pilot.getInstance(), path+"3.femaleSdAgeAtMaturation", femaleSdAgeAtMaturation);
ReflectUtils.setFieldValueFromPath(Pilot.getInstance(), path+"3.femaleMeanInterSpawningInterval", femaleMeanInterSpawningInterval);
ReflectUtils.setFieldValueFromPath(Pilot.getInstance(), path+"3.maleMeanAgeAtMaturation", maleMeanAgeAtMaturation );
ReflectUtils.setFieldValueFromPath(Pilot.getInstance(), path+"3.maleSdAgeAtMaturation", maleSdAgeAtMaturation);
ReflectUtils.setFieldValueFromPath(Pilot.getInstance(), path+"3.maleMeanInterSpawningInterval", maleMeanInterSpawningInterval);
ReflectUtils.setFieldValueFromPath(Pilot.getInstance(), path+"4.probabilityToBirthHabitat", probabilityToBirthHabitat);
ReflectUtils.setFieldValueFromPath(Pilot.getInstance(), path+"4.probabilityToBirthRiver", probabilityToBirthRiver);
ReflectUtils.setFieldValueFromPath(Pilot.getInstance(), path+"5.fecundity", fecundity);
ReflectUtils.setFieldValueFromPath(Pilot.getInstance(), path+"5.cvRecruitment", cvRecruitment);
ReflectUtils.setFieldValueFromPath(Pilot.getInstance(), path+"7.M0_3months", M0_3months);
ReflectUtils.setFieldValueFromPath(Pilot.getInstance(), path+"7.cvM0_3months", cvM0_3months);
ReflectUtils.setFieldValueFromPath(Pilot.getInstance(), path+"7.M3_12months", M3_12months);
ReflectUtils.setFieldValueFromPath(Pilot.getInstance(), path+"7.cvM3_12months", cvM3_12months);
ReflectUtils.setFieldValueFromPath(Pilot.getInstance(), path+"7.M1year", M1year);
ReflectUtils.setFieldValueFromPath(Pilot.getInstance(), path+"7.cvM1year", cvM1year);
ReflectUtils.setFieldValueFromPath(Pilot.getInstance(), path+"7.Mafter", Mafter);
ReflectUtils.setFieldValueFromPath(Pilot.getInstance(), path+"7.cvMafter", cvMafter);
//ReflectUtils.setFieldValueFromPath(Pilot.getInstance(), path+"9.releaseNumber", releaseAmount);
BatchRunner.load();

Pilot.run()
//femaleSpawnerInOceanEffective = ReflectUtils.getValueFromPath(Pilot.getInstance(),"aquaticWorld.aquaNismsGroupsList.0.getFemaleSpawnerInOceanEffective");
//wildActiveFemaleSpawner = ReflectUtils.getValueFromPath(Pilot.getInstance(),path+"5.getNbWildActiveFemaleSpawner");
//meanWildActiveFemaleSpawner = ReflectUtils.getValueFromPath(Pilot.getInstance(),path+"5.getMeanLastWildActiveFemaleSpawnerNumber");
//lastFailInReproduction = ReflectUtils.getValueFromPath(Pilot.getInstance(),path+"5.getTimeOfLastFemaleNumberFail");
reproductionNumber = ReflectUtils.getValueFromPath(Pilot.getInstance(),path+"5.getReproductionNumber");
timeStepStop = ReflectUtils.getValueFromPath(Pilot.getInstance(),path+"5.getTimeStepStop");
geoMeanEffective = ReflectUtils.getValueFromPath(Pilot.getInstance(),path+"10.getGeometricMeanFisnNumber");
